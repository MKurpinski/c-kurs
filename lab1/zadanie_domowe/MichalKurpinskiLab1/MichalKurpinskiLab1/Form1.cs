﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiLab1
{
    public partial class Form1 : Form
    {
        int number = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void textBoxOpen_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void buttonOpen_Click(object sender, EventArgs e)
        {
            buttonOpen.BackColor = Color.Aqua;

            // MessageBox.Show("Wartość: " + textBoxOpen.Text);
            if (number+7< 50)
            {
                number += 7;
                textBoxOpen.Text = number.ToString();
               // MessageBox.Show(number.ToString());
            }
            else {
                textBoxOpen.Text = "Osiągnieto limit";
            }
            
            textBoxNumber1.Text = number.ToString();
            textBoxNumber2.Text = (number + 7).ToString();
            textBoxTimes.Text = (number * (number + 7)).ToString();
            FormNewWindow formNewWindow = new FormNewWindow();
            formNewWindow.lastNumber = Int32.Parse(textBoxOpen.Text);
            formNewWindow.ShowDialog();

        }

        private void textBoxNumber1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
