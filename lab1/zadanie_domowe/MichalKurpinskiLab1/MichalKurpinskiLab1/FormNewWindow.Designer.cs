﻿namespace MichalKurpinskiLab1
{
    partial class FormNewWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonShow = new System.Windows.Forms.Button();
            this.textBoxDraw = new System.Windows.Forms.TextBox();
            this.buttonDraw = new System.Windows.Forms.Button();
            this.textBoxShow = new System.Windows.Forms.TextBox();
            this.buttonExit = new System.Windows.Forms.Button();
            this.textBoxHowMany = new System.Windows.Forms.TextBox();
            this.labelHeightOfTree = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonShow
            // 
            this.buttonShow.Location = new System.Drawing.Point(100, 53);
            this.buttonShow.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.buttonShow.Name = "buttonShow";
            this.buttonShow.Size = new System.Drawing.Size(127, 68);
            this.buttonShow.TabIndex = 0;
            this.buttonShow.Text = "Show";
            this.buttonShow.UseVisualStyleBackColor = true;
            this.buttonShow.Click += new System.EventHandler(this.buttonShow_Click);
            // 
            // textBoxDraw
            // 
            this.textBoxDraw.Location = new System.Drawing.Point(308, 172);
            this.textBoxDraw.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxDraw.Multiline = true;
            this.textBoxDraw.Name = "textBoxDraw";
            this.textBoxDraw.Size = new System.Drawing.Size(745, 336);
            this.textBoxDraw.TabIndex = 1;
            // 
            // buttonDraw
            // 
            this.buttonDraw.Location = new System.Drawing.Point(100, 283);
            this.buttonDraw.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.buttonDraw.Name = "buttonDraw";
            this.buttonDraw.Size = new System.Drawing.Size(127, 68);
            this.buttonDraw.TabIndex = 2;
            this.buttonDraw.Text = "Draw";
            this.buttonDraw.UseVisualStyleBackColor = true;
            this.buttonDraw.Click += new System.EventHandler(this.buttonDraw_Click);
            // 
            // textBoxShow
            // 
            this.textBoxShow.Location = new System.Drawing.Point(308, 70);
            this.textBoxShow.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxShow.Name = "textBoxShow";
            this.textBoxShow.Size = new System.Drawing.Size(443, 35);
            this.textBoxShow.TabIndex = 3;
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(914, 575);
            this.buttonExit.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(203, 78);
            this.buttonExit.TabIndex = 4;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // textBoxHowMany
            // 
            this.textBoxHowMany.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxHowMany.Location = new System.Drawing.Point(100, 236);
            this.textBoxHowMany.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxHowMany.Name = "textBoxHowMany";
            this.textBoxHowMany.Size = new System.Drawing.Size(127, 41);
            this.textBoxHowMany.TabIndex = 6;
            // 
            // labelHeightOfTree
            // 
            this.labelHeightOfTree.AutoSize = true;
            this.labelHeightOfTree.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelHeightOfTree.Location = new System.Drawing.Point(95, 186);
            this.labelHeightOfTree.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelHeightOfTree.Name = "labelHeightOfTree";
            this.labelHeightOfTree.Size = new System.Drawing.Size(153, 29);
            this.labelHeightOfTree.TabIndex = 7;
            this.labelHeightOfTree.Text = "height of tree";
            // 
            // FormNewWindow
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1238, 749);
            this.Controls.Add(this.labelHeightOfTree);
            this.Controls.Add(this.textBoxHowMany);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.textBoxShow);
            this.Controls.Add(this.buttonDraw);
            this.Controls.Add(this.textBoxDraw);
            this.Controls.Add(this.buttonShow);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ForeColor = System.Drawing.SystemColors.Desktop;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "FormNewWindow";
            this.ShowInTaskbar = false;
            this.Text = "FormNewWindom";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonShow;
        private System.Windows.Forms.TextBox textBoxDraw;
        private System.Windows.Forms.Button buttonDraw;
        private System.Windows.Forms.TextBox textBoxShow;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.TextBox textBoxHowMany;
        private System.Windows.Forms.Label labelHeightOfTree;
    }
}