﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiLab1
{
    public partial class FormNewWindow : Form
    {
       public int lastNumber=0;
        public FormNewWindow()
        {
            InitializeComponent();
        }

        private void buttonShow_Click(object sender, EventArgs e)
        {
            textBoxShow.Text = lastNumber.ToString();
        }


        private void buttonDraw_Click(object sender, EventArgs e)
        {
            int length = 0;

            int.TryParse(textBoxHowMany.Text, out length);
            if (length == 0) {
                textBoxHowMany.Text = "";
                MessageBox.Show("Zła wartosc!");
            }
            for(int i = 0; i < length; i++)
            {
                if (i == 0)
                {
                    for (int k = 0; k < length; k++)
                    {
                        textBoxDraw.Text += " ";
                    }
                    textBoxDraw.Text += "*";
                   
                }
                for (int k = 0; k < length - i; k++)
                {
                    textBoxDraw.Text += " ";
                }
                for (int j = 0; j < 2*i; j++)
                {
                   
                    textBoxDraw.Text += "*";
                    
                }
                textBoxDraw.Text += Environment.NewLine;
            }
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}
