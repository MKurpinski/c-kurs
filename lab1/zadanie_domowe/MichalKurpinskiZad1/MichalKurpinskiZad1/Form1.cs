﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.Windows.Forms;

namespace MichalKurpinskiZad1
{
    public partial class Form1 : Form
    {
        bool mute = false;
        bool operPressed = false;
        Double value = 0;
        SoundPlayer fanfare = new SoundPlayer(Properties.Resources.Fanfary);
        SystemSound error = SystemSounds.Hand;
        string operation = "";
        Random r = new Random();
        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Metoda opisuje zachowanie się aplikacji po kliknięciu przycisków 0-9. Dodanie odpowiedniej liczby do textBoxaResult, oraz sprawdzenie
        /// warunku, czy nie został przekroczony zakres wpisywania liczb.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Click(Object sender, EventArgs e)
        {
            if (textBoxResult.Text.Equals("0") || operPressed) {
                textBoxResult.Clear();
            }
                operPressed = false;
                Button b = (Button)sender;
            if (textBoxResult.Text.Length + 1 > 13)
            {
                if (!mute)
                {
                    error.Play();
                }
                
                MessageBox.Show("Przekroczyłes zakres długości liczby!", "Niewielki błąd ;)");
            }
            else
            {
                textBoxResult.Text += b.Text;
            }
          
            
        }
        /// <summary>
        /// Zachowanie funkcji po naciśnieciu przycisku kropki.Jeśli, już znajduję się przecinek w naszym wyrażeniu nic się nie dzieje,
        /// w odwrotnym przypadku jest on dodawany.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDot_Click(object sender, EventArgs e)
        {
            if (textBoxResult.Text.Contains("."))
            {
                return;
            }
            else
            {
                if (textBoxResult.Text.Equals("0"))
                {
                    textBoxResult.Text = "0,";
                }
                else {
                    textBoxResult.Text += ",";
                }
            }

        }
        /// <summary>
        /// Zachowanie programu po nasiśnieciu przycisku CE, czyli wyczyścienie naszego kalkulatora do pierwotnego stanu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCE_Click(object sender, EventArgs e)
        {
            textBoxResult.Text = "0";
            labelEquality.Text = "";
            operation = "";
            value = 0;
        }
        /// <summary>
        /// Zachowanie programu po naciśnieciu przypisku odpowiadającemu jednemu z operatorów. Rózne zachowanie programu 
        /// uwarunkowane jest poprzez instrukcję switch.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Operator_Click(object sender, EventArgs e)
        {
            if (operPressed) {
                return;
            }
            Button b = (Button)sender;
            operPressed= true;
            operation = b.Text;
            value = double.Parse(textBoxResult.Text);
            switch (operation)
            {
                case "√":
                    labelEquality.Text = "√("+value+")";
                    break;
                case "1/x":
                    labelEquality.Text = "1/"+value;
                    break;
                case "ln":
                    labelEquality.Text = "ln(" + value+")";
                    break;
                default:
                    labelEquality.Text = value + operation;
                    break;
            }
            
        }
        /// <summary>
        /// Zachowanie programu po naciśnieciu przycisku wyboru znaku liczby. Dodaje znak "-" przed liczbą lub go usuwa.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonSign_Click(object sender, EventArgs e)
        {
            if (textBoxResult.Text.Contains("-"))
            {
                textBoxResult.Text = textBoxResult.Text.Substring(1);
            }
            else
            {
                textBoxResult.Text = "-" + textBoxResult.Text;
            }
        }
        /// <summary>
        /// Zachowanie programu po naciśnieciu przycisku =. Odpowiednią operację warunkuje nam jej operator.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEqual_Click(object sender, EventArgs e)
        {
            buttonEqual.BackColor = Color.FromArgb(r.Next());
            operPressed = false;
            labelEquality.Text = "";
            switch (operation) {
                case "+" :
                    textBoxResult.Text = (value + Double.Parse(textBoxResult.Text)).ToString();
                    break;
                case "-":
                    textBoxResult.Text = (value - Double.Parse(textBoxResult.Text)).ToString();
                    break;
                case "*":
                    textBoxResult.Text = (value * Double.Parse(textBoxResult.Text)).ToString();
                    break;
                case "/":
                    Double d = Double.Parse(textBoxResult.Text);
                    if (d==0)
                    {
                        if (!mute)
                        {
                            error.Play();
                        }
                        MessageBox.Show("Nie można dzielić przez zero!", "Wstydż się :)");
                        return;
                    }
                    
                        textBoxResult.Text = (value / Double.Parse(textBoxResult.Text)).ToString();
                    
                    break;
                case "^":
                    textBoxResult.Text = (Math.Pow(value, Double.Parse(textBoxResult.Text))).ToString();
                    break;
                case "1/x":
                     if (value == 0)
                     {
                        if (!mute)
                        {
                            error.Play();
                        }
                        MessageBox.Show("Nie można dzielić przez zero!", "Wstydż się :)");
                        return;
                     }
                     
                          textBoxResult.Text = (1/value).ToString();
                    break;
                case "√":
                    if (value > 0)
                    {
                        textBoxResult.Text = Math.Sqrt(value).ToString();
                    }
                    else
                    {
                        if (!mute)
                        {
                            error.Play();
                        }
                        MessageBox.Show("Nieprawidłowe dane wejściowe", "Popraw się!");
                        buttonCE_Click(buttonC, e);
                        return;
                    }
                    break;
                case "!":
                    if (value < 0.51)
                    {
                        if (!mute)
                        {
                            error.Play();
                            
                        }
                        MessageBox.Show("Nieprawidłowe dane wejsciowe", "Popraw się!");
                        buttonCE_Click(buttonC, e);
                        return;
                    }
                   
                        int val = Convert.ToInt32(value);
                        int result = 1;
                        for (int i = 1; i <= val; i++)
                        {
                            result *= i;
                        }
                        textBoxResult.Text = result.ToString(); 
                    
                    break;
                case "ln":
                    if (value < 0)
                    {
                        if (!mute)
                        {
                            error.Play();
                        }
                        MessageBox.Show("Nieprawidłowe dane wejsciowe", "Popraw się!");
                        buttonCE_Click(buttonC, e);
                        return;
                    }
                   
                        textBoxResult.Text = Math.Log(value).ToString();
                    
                    break;
                   

            }
            if (!mute)
            {
               fanfare.Play();
            }
        }
        /// <summary>
        /// Zachowanie programu po naciśnieciu prycisku C. Usunięcie ostatniej wprowadzonej liczby.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonC_Click(object sender, EventArgs e)
        {
            if (textBoxResult.Text.Length == 1)
            {
                textBoxResult.Text = "0";
            }
            else
            {
                textBoxResult.Text = textBoxResult.Text.Substring(1);
            }
        }
        /// <summary>
        /// Przycisk umożliwiający wyciszenie aplikacji.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMute_Click(object sender, EventArgs e)
        {
            if (mute)
            {
                mute = false;
                buttonMute.BackgroundImage = Properties.Resources.music;
            }
            else
            {
                mute = true;
                fanfare.Stop();
                buttonMute.BackgroundImage = Properties.Resources.mute;
            }
        }
    }
    }

