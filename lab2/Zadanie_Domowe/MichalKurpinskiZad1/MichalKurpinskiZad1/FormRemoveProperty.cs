﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiZad1
{
    public partial class FormRemoveProperty : Form
    {
        FormMain owner;//"własciciel" tej formy.
        public FormRemoveProperty(FormMain owner)
        {
            InitializeComponent();
            this.owner = owner;
        }
        private void buttonNo_Click(object sender, EventArgs e)
        {
            FormAfterClickingNo formAfterClickingNo = new FormAfterClickingNo();
            formAfterClickingNo.ShowDialog();
            this.Close();
        }
        /// <summary>
        /// Potwierdzenie usunięcia specjalnej własciwosći.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonYes_Click(object sender, EventArgs e)
        {
            try
            {
                owner.listOfClubs[owner.dataGridViewClubs.SelectedRows[0].Index].properties.RemoveAt(owner.dataGridViewsSpecialProperty.SelectedRows[0].Index);
                owner.dataGridViewsSpecialProperty.DataSource = null;
                owner.dataGridViewsSpecialProperty.DataSource = owner.listOfClubs[owner.dataGridViewClubs.SelectedRows[0].Index].properties;
                MessageBox.Show("Usuwanie powiodło się:)", ":)");
                this.Close();
            }
            catch (Exception)
            {
                owner.error.Play();
                MessageBox.Show("Usuwanie nie powiodło się!", "Error");
            }
        }
    }
}
