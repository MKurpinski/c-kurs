﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiZad1
{
    public partial class DeleteForm : Form
    {
        FormMain owner;//"wlaściciel" tej formy, pozwala nam na odwoływanie się do elementów naszej głownej formy.
        public DeleteForm(FormMain owner)
        {
            InitializeComponent();
            this.owner = owner;
        }
        /// <summary>
        /// Anuluje usuwanie zaznaczonego klubu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonNo_Click(object sender, EventArgs e)
        {
            FormAfterClickingNo formAfterClickingNo = new FormAfterClickingNo();
            formAfterClickingNo.ShowDialog();
            this.Close();
        }
        /// <summary>
        /// potwierdza usunięcie zaznaczonego klubu oraz aktualizuje DataGridViewLisOfClubs.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonYes_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Klub :" + owner.listOfClubs[owner.dataGridViewClubs.SelectedRows[0].Index].Name + " został pomyślnie usunięty", "Udało się");
            owner.listOfClubs.RemoveAt(owner.dataGridViewClubs.SelectedRows[0].Index);
            owner.dataGridViewClubs.DataSource = null;
            owner.dataGridViewClubs.DataSource = owner.listOfClubs;
            owner.dataGridViewAchievements.DataSource = null;
            owner.dataGridViewsSpecialProperty.DataSource = null;
            owner.bravo.Play();
            this.Close();
        }
    }
}

