﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MichalKurpinskiZad1
{
    [Serializable]
    public class Property
    {
        public String SpecialProperty { get; set; }
        public Property() { }
        public Property(string mess)
        {
            SpecialProperty = mess;
        }
    }
}
