﻿namespace MichalKurpinskiZad1
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.buttonAddClub = new System.Windows.Forms.Button();
            this.buttonAddAchievements = new System.Windows.Forms.Button();
            this.buttonOpen = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonAddSpecialProperty = new System.Windows.Forms.Button();
            this.dataGridViewClubs = new System.Windows.Forms.DataGridView();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelListOfClubs = new System.Windows.Forms.Label();
            this.dataGridViewAchievements = new System.Windows.Forms.DataGridView();
            this.labelAchievements = new System.Windows.Forms.Label();
            this.dataGridViewsSpecialProperty = new System.Windows.Forms.DataGridView();
            this.labelSpecialProperties = new System.Windows.Forms.Label();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.pictureBoxFlag = new System.Windows.Forms.PictureBox();
            this.buttonWatch = new System.Windows.Forms.Button();
            this.buttonDeleteProperty = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewClubs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAchievements)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewsSpecialProperty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFlag)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonAddClub
            // 
            this.buttonAddClub.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddClub.Location = new System.Drawing.Point(126, 1001);
            this.buttonAddClub.Name = "buttonAddClub";
            this.buttonAddClub.Size = new System.Drawing.Size(248, 134);
            this.buttonAddClub.TabIndex = 1;
            this.buttonAddClub.Text = "Add Club";
            this.buttonAddClub.UseVisualStyleBackColor = true;
            this.buttonAddClub.Click += new System.EventHandler(this.buttonAddClub_Click);
            // 
            // buttonAddAchievements
            // 
            this.buttonAddAchievements.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddAchievements.Location = new System.Drawing.Point(717, 1001);
            this.buttonAddAchievements.Name = "buttonAddAchievements";
            this.buttonAddAchievements.Size = new System.Drawing.Size(248, 134);
            this.buttonAddAchievements.TabIndex = 2;
            this.buttonAddAchievements.Text = "Add achievement";
            this.buttonAddAchievements.UseVisualStyleBackColor = true;
            this.buttonAddAchievements.Click += new System.EventHandler(this.buttonAddAchievements_Click);
            // 
            // buttonOpen
            // 
            this.buttonOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonOpen.Location = new System.Drawing.Point(1644, 1001);
            this.buttonOpen.Name = "buttonOpen";
            this.buttonOpen.Size = new System.Drawing.Size(248, 134);
            this.buttonOpen.TabIndex = 3;
            this.buttonOpen.Text = "Open";
            this.buttonOpen.UseVisualStyleBackColor = true;
            this.buttonOpen.Click += new System.EventHandler(this.buttonOpen_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSave.Location = new System.Drawing.Point(1988, 1001);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(248, 134);
            this.buttonSave.TabIndex = 4;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonAddSpecialProperty
            // 
            this.buttonAddSpecialProperty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddSpecialProperty.Location = new System.Drawing.Point(1041, 1001);
            this.buttonAddSpecialProperty.Name = "buttonAddSpecialProperty";
            this.buttonAddSpecialProperty.Size = new System.Drawing.Size(248, 134);
            this.buttonAddSpecialProperty.TabIndex = 5;
            this.buttonAddSpecialProperty.Text = "Add special property";
            this.buttonAddSpecialProperty.UseVisualStyleBackColor = true;
            this.buttonAddSpecialProperty.Click += new System.EventHandler(this.buttonAddSpecialProperty_Click);
            // 
            // dataGridViewClubs
            // 
            this.dataGridViewClubs.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewClubs.BackgroundColor = System.Drawing.Color.PaleGoldenrod;
            this.dataGridViewClubs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewClubs.Location = new System.Drawing.Point(126, 220);
            this.dataGridViewClubs.MultiSelect = false;
            this.dataGridViewClubs.Name = "dataGridViewClubs";
            this.dataGridViewClubs.ReadOnly = true;
            this.dataGridViewClubs.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataGridViewClubs.RowTemplate.Height = 37;
            this.dataGridViewClubs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewClubs.Size = new System.Drawing.Size(986, 732);
            this.dataGridViewClubs.TabIndex = 6;
            this.dataGridViewClubs.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewClubs_RowHeaderMouseClick);
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 23F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTitle.Location = new System.Drawing.Point(888, 21);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(488, 79);
            this.labelTitle.TabIndex = 8;
            this.labelTitle.Text = "Football Clubs";
            // 
            // labelListOfClubs
            // 
            this.labelListOfClubs.AutoSize = true;
            this.labelListOfClubs.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.labelListOfClubs.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelListOfClubs.Location = new System.Drawing.Point(456, 133);
            this.labelListOfClubs.Name = "labelListOfClubs";
            this.labelListOfClubs.Size = new System.Drawing.Size(275, 52);
            this.labelListOfClubs.TabIndex = 9;
            this.labelListOfClubs.Text = "List of clubs";
            // 
            // dataGridViewAchievements
            // 
            this.dataGridViewAchievements.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewAchievements.BackgroundColor = System.Drawing.Color.PaleGoldenrod;
            this.dataGridViewAchievements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAchievements.Location = new System.Drawing.Point(1191, 601);
            this.dataGridViewAchievements.Name = "dataGridViewAchievements";
            this.dataGridViewAchievements.RowTemplate.Height = 37;
            this.dataGridViewAchievements.Size = new System.Drawing.Size(1045, 351);
            this.dataGridViewAchievements.TabIndex = 10;
            // 
            // labelAchievements
            // 
            this.labelAchievements.AutoSize = true;
            this.labelAchievements.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.labelAchievements.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAchievements.Location = new System.Drawing.Point(1542, 537);
            this.labelAchievements.Name = "labelAchievements";
            this.labelAchievements.Size = new System.Drawing.Size(319, 52);
            this.labelAchievements.TabIndex = 11;
            this.labelAchievements.Text = "Achievements";
            // 
            // dataGridViewsSpecialProperty
            // 
            this.dataGridViewsSpecialProperty.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewsSpecialProperty.BackgroundColor = System.Drawing.Color.PaleGoldenrod;
            this.dataGridViewsSpecialProperty.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewsSpecialProperty.Location = new System.Drawing.Point(1191, 220);
            this.dataGridViewsSpecialProperty.Name = "dataGridViewsSpecialProperty";
            this.dataGridViewsSpecialProperty.RowTemplate.Height = 37;
            this.dataGridViewsSpecialProperty.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewsSpecialProperty.Size = new System.Drawing.Size(1045, 299);
            this.dataGridViewsSpecialProperty.TabIndex = 12;
            // 
            // labelSpecialProperties
            // 
            this.labelSpecialProperties.AutoSize = true;
            this.labelSpecialProperties.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.labelSpecialProperties.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSpecialProperties.Location = new System.Drawing.Point(1507, 133);
            this.labelSpecialProperties.Name = "labelSpecialProperties";
            this.labelSpecialProperties.Size = new System.Drawing.Size(402, 52);
            this.labelSpecialProperties.TabIndex = 13;
            this.labelSpecialProperties.Text = "Special properties";
            // 
            // buttonDelete
            // 
            this.buttonDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonDelete.Location = new System.Drawing.Point(419, 1001);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(248, 134);
            this.buttonDelete.TabIndex = 14;
            this.buttonDelete.Text = "Delete club";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // pictureBoxFlag
            // 
            this.pictureBoxFlag.BackgroundImage = global::MichalKurpinskiZad1.Properties.Resources.trawa;
            this.pictureBoxFlag.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxFlag.Location = new System.Drawing.Point(0, 3);
            this.pictureBoxFlag.Name = "pictureBoxFlag";
            this.pictureBoxFlag.Size = new System.Drawing.Size(273, 214);
            this.pictureBoxFlag.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFlag.TabIndex = 7;
            this.pictureBoxFlag.TabStop = false;
            // 
            // buttonWatch
            // 
            this.buttonWatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonWatch.Location = new System.Drawing.Point(2051, 21);
            this.buttonWatch.Name = "buttonWatch";
            this.buttonWatch.Size = new System.Drawing.Size(258, 134);
            this.buttonWatch.TabIndex = 15;
            this.buttonWatch.Text = "VIDEO! :)";
            this.buttonWatch.UseVisualStyleBackColor = true;
            this.buttonWatch.Click += new System.EventHandler(this.buttonWatch_Click);
            // 
            // buttonDeleteProperty
            // 
            this.buttonDeleteProperty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonDeleteProperty.Location = new System.Drawing.Point(1337, 1001);
            this.buttonDeleteProperty.Name = "buttonDeleteProperty";
            this.buttonDeleteProperty.Size = new System.Drawing.Size(248, 134);
            this.buttonDeleteProperty.TabIndex = 16;
            this.buttonDeleteProperty.Text = "Delete Property";
            this.buttonDeleteProperty.UseVisualStyleBackColor = true;
            this.buttonDeleteProperty.Click += new System.EventHandler(this.buttonDeleteProperty_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackgroundImage = global::MichalKurpinskiZad1.Properties.Resources.trawa;
            this.ClientSize = new System.Drawing.Size(2347, 1177);
            this.Controls.Add(this.buttonDeleteProperty);
            this.Controls.Add(this.buttonWatch);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.labelSpecialProperties);
            this.Controls.Add(this.dataGridViewsSpecialProperty);
            this.Controls.Add(this.labelAchievements);
            this.Controls.Add(this.dataGridViewAchievements);
            this.Controls.Add(this.labelListOfClubs);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.pictureBoxFlag);
            this.Controls.Add(this.dataGridViewClubs);
            this.Controls.Add(this.buttonAddSpecialProperty);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonOpen);
            this.Controls.Add(this.buttonAddAchievements);
            this.Controls.Add(this.buttonAddClub);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "Football Clubs";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewClubs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAchievements)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewsSpecialProperty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFlag)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonAddClub;
        private System.Windows.Forms.Button buttonAddAchievements;
        private System.Windows.Forms.Button buttonOpen;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonAddSpecialProperty;
        private System.Windows.Forms.PictureBox pictureBoxFlag;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelListOfClubs;
        private System.Windows.Forms.Label labelAchievements;
        private System.Windows.Forms.Label labelSpecialProperties;
        public System.Windows.Forms.DataGridView dataGridViewClubs;
        public System.Windows.Forms.DataGridView dataGridViewAchievements;
        public System.Windows.Forms.DataGridView dataGridViewsSpecialProperty;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonWatch;
        private System.Windows.Forms.Button buttonDeleteProperty;
    }
}

