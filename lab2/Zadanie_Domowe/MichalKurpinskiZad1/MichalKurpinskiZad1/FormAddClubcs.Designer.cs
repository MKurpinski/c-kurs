﻿namespace MichalKurpinskiZad1
{
    partial class FormAddClubcs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAddClubcs));
            this.labelTitle = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxLocalization = new System.Windows.Forms.TextBox();
            this.textBoxStadium = new System.Windows.Forms.TextBox();
            this.textBoxYear = new System.Windows.Forms.TextBox();
            this.buttonAddIEmblemat = new System.Windows.Forms.Button();
            this.buttonConfirm = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTitle.Location = new System.Drawing.Point(223, 62);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(412, 59);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "Add FootballClub";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(97, 140);
            this.textBoxName.Multiline = true;
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(662, 68);
            this.textBoxName.TabIndex = 1;
            this.textBoxName.Text = "Name...";
            this.textBoxName.Click += new System.EventHandler(this.textBoxName_Click);
            // 
            // textBoxLocalization
            // 
            this.textBoxLocalization.Location = new System.Drawing.Point(97, 250);
            this.textBoxLocalization.Multiline = true;
            this.textBoxLocalization.Name = "textBoxLocalization";
            this.textBoxLocalization.Size = new System.Drawing.Size(662, 68);
            this.textBoxLocalization.TabIndex = 2;
            this.textBoxLocalization.Text = "Localization..";
            this.textBoxLocalization.Click += new System.EventHandler(this.textBoxName_Click);
            // 
            // textBoxStadium
            // 
            this.textBoxStadium.Location = new System.Drawing.Point(97, 351);
            this.textBoxStadium.Multiline = true;
            this.textBoxStadium.Name = "textBoxStadium";
            this.textBoxStadium.Size = new System.Drawing.Size(662, 68);
            this.textBoxStadium.TabIndex = 3;
            this.textBoxStadium.Text = "Stadium...";
            this.textBoxStadium.Click += new System.EventHandler(this.textBoxName_Click);
            // 
            // textBoxYear
            // 
            this.textBoxYear.Location = new System.Drawing.Point(97, 459);
            this.textBoxYear.Multiline = true;
            this.textBoxYear.Name = "textBoxYear";
            this.textBoxYear.Size = new System.Drawing.Size(662, 68);
            this.textBoxYear.TabIndex = 4;
            this.textBoxYear.Text = "Founded(Year)...";
            this.textBoxYear.Click += new System.EventHandler(this.textBoxName_Click);
            // 
            // buttonAddIEmblemat
            // 
            this.buttonAddIEmblemat.Location = new System.Drawing.Point(42, 618);
            this.buttonAddIEmblemat.Name = "buttonAddIEmblemat";
            this.buttonAddIEmblemat.Size = new System.Drawing.Size(263, 72);
            this.buttonAddIEmblemat.TabIndex = 5;
            this.buttonAddIEmblemat.Text = "Load emblem";
            this.buttonAddIEmblemat.UseVisualStyleBackColor = true;
            this.buttonAddIEmblemat.Click += new System.EventHandler(this.buttonAddIEmblemat_Click);
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.Location = new System.Drawing.Point(311, 618);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(263, 72);
            this.buttonConfirm.TabIndex = 6;
            this.buttonConfirm.Text = "Confirm";
            this.buttonConfirm.UseVisualStyleBackColor = true;
            this.buttonConfirm.Click += new System.EventHandler(this.buttonConfirm_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(580, 618);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(263, 72);
            this.buttonCancel.TabIndex = 7;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // FormAddClubcs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MichalKurpinskiZad1.Properties.Resources.trawa;
            this.ClientSize = new System.Drawing.Size(869, 745);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonConfirm);
            this.Controls.Add(this.buttonAddIEmblemat);
            this.Controls.Add(this.textBoxYear);
            this.Controls.Add(this.textBoxStadium);
            this.Controls.Add(this.textBoxLocalization);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelTitle);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormAddClubcs";
            this.Text = "Add Club";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxLocalization;
        private System.Windows.Forms.TextBox textBoxStadium;
        private System.Windows.Forms.TextBox textBoxYear;
        private System.Windows.Forms.Button buttonAddIEmblemat;
        private System.Windows.Forms.Button buttonConfirm;
        private System.Windows.Forms.Button buttonCancel;
    }
}