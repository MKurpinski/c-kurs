﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace MichalKurpinskiZad1
{
    public partial class FormMain : Form
    {
        public SoundPlayer bravo = new SoundPlayer(Properties.Resources.applause_2);
        public SystemSound error = SystemSounds.Hand;
        public List<Club> listOfClubs = new List<Club>();
        XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Club>));

        public FormMain()
        {
            InitializeComponent();
        }

        private void buttonShowClubs_Click(object sender, EventArgs e)
        {
            
            dataGridViewClubs.DataSource = null;  
            dataGridViewClubs.DataSource = listOfClubs;



        }
        /// <summary>
        /// otwieranie listy klubów z pliku xml.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "XMLFiles(*.xml)\"|*.xml";
            openFileDialog.Title = "Open list of clubs from file...";
            openFileDialog.FilterIndex = 1;
            openFileDialog.ShowDialog();
            string openFileName = openFileDialog.FileName;
            StreamReader reader;
            try
            {
                reader = new StreamReader(openFileName);
                listOfClubs = (List<Club>) xmlSerializer.Deserialize(reader);
                bravo.Play();
                dataGridViewClubs.DataSource = listOfClubs;
                reader.Close();

            }
            catch (Exception ) {
              
                }
            
            
        }
        /// <summary>
        /// zapisywanie listy klubów do pliku xml.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "XMLFiles(*.xml)\"|*.xml";
            saveFileDialog.Title = "Save your list of clubs to file...";
            saveFileDialog.AddExtension = true;
            saveFileDialog.ShowDialog();
            string saveFileName = saveFileDialog.FileName;
            try
            {
                StreamWriter writer = new StreamWriter(saveFileName);
                xmlSerializer.Serialize(writer, listOfClubs);
                writer.Flush();
                writer.Close();
                writer.Dispose();
            }
            catch (Exception)
            {
            }

        }
        /// <summary>
        /// Przycisk otwierający okno służace do dodawania klubów.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddClub_Click(object sender, EventArgs e)
        {
            FormAddClubcs formAddClubs = new FormAddClubcs(this);
            formAddClubs.ShowDialog();
        }
        /// <summary>
        /// Przycisk otwierający okno służace do dodawania specjalnych własciwości.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddSpecialProperty_Click(object sender, EventArgs e)
        {
            if (dataGridViewClubs.SelectedRows.Count > 0) { 
            FormSpecialProperty specialProperty = new FormSpecialProperty(this);
            specialProperty.ShowDialog();
            }
            else
            {
                error.Play();
                MessageBox.Show("Nie zaznaczyłeś żadnego klubu!", "Error");
            }
        }
        /// <summary>
        /// Przycisk otwierający okno służace do dodawania osiągnieć.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddAchievements_Click(object sender, EventArgs e)
        {
            if (dataGridViewClubs.SelectedRows.Count > 0) { 
            FormAchievements formAchiements = new FormAchievements(this);
            formAchiements.ShowDialog();
             }
            else
            {
                error.Play();
                MessageBox.Show("Nie zaznaczyłeś żadnego klubu!", "Error");
            }

        }
        /// <summary>
        /// Przycisk otwierający okno służace do usuwania klubów.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridViewClubs.SelectedRows.Count > 0)
            {
                DeleteForm deleteForm = new DeleteForm(this);
                deleteForm.ShowDialog();
            }
            else
            {
                error.Play();
                MessageBox.Show("Nie zaznaczyłeś żadnego klubu!", "Error");
            }
            
        }
        /// <summary>
        /// Przycisk odpowiedzialny za ustawiania źródła dla DataGridView odpowiedzialnych za własciwości i osiągniecia.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewClubs_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            dataGridViewAchievements.DataSource = null;
            dataGridViewAchievements.DataSource = listOfClubs[dataGridViewClubs.SelectedRows[0].Index].achieves;
            dataGridViewsSpecialProperty.DataSource = null;
            dataGridViewsSpecialProperty.DataSource = listOfClubs[dataGridViewClubs.SelectedRows[0].Index].properties;
            pictureBoxFlag.Image = listOfClubs[dataGridViewClubs.SelectedRows[0].Index].Emblem;
        }
        /// <summary>
        /// Otwiera okno służace do oglądania jednego z 5 filmików z Yt.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonWatch_Click(object sender, EventArgs e)
        {
            FormWatch formWatch = new FormWatch();
            formWatch.ShowDialog();
        }
        /// <summary>
        /// Przycisk otwierający okno usuwania specjalnej własciwosci.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDeleteProperty_Click(object sender, EventArgs e)
        {
            if (dataGridViewsSpecialProperty.SelectedRows.Count > 0) { 
            FormRemoveProperty formRemoveProperty = new FormRemoveProperty(this);
            formRemoveProperty.Show();
            }
            else
            {
                error.Play();
                MessageBox.Show("Nie zaznaczyłeś żadnego wlasciwosci!", "Error");
            }
        }
    }
    }

