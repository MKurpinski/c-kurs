﻿namespace MichalKurpinskiZad1
{
    partial class FormWatch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormWatch));
            this.axShockwaveFlashVideo = new AxShockwaveFlashObjects.AxShockwaveFlash();
            this.buttonWatch = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.axShockwaveFlashVideo)).BeginInit();
            this.SuspendLayout();
            // 
            // axShockwaveFlashVideo
            // 
            this.axShockwaveFlashVideo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axShockwaveFlashVideo.Enabled = true;
            this.axShockwaveFlashVideo.Location = new System.Drawing.Point(0, 0);
            this.axShockwaveFlashVideo.Name = "axShockwaveFlashVideo";
            this.axShockwaveFlashVideo.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axShockwaveFlashVideo.OcxState")));
            this.axShockwaveFlashVideo.Size = new System.Drawing.Size(1566, 1008);
            this.axShockwaveFlashVideo.TabIndex = 0;
            // 
            // buttonWatch
            // 
            this.buttonWatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonWatch.ForeColor = System.Drawing.Color.Red;
            this.buttonWatch.Location = new System.Drawing.Point(1135, 0);
            this.buttonWatch.Name = "buttonWatch";
            this.buttonWatch.Size = new System.Drawing.Size(431, 89);
            this.buttonWatch.TabIndex = 1;
            this.buttonWatch.Text = "Watch!";
            this.buttonWatch.UseVisualStyleBackColor = true;
            this.buttonWatch.Click += new System.EventHandler(this.buttonWatch_Click);
            // 
            // FormWatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1566, 1008);
            this.Controls.Add(this.buttonWatch);
            this.Controls.Add(this.axShockwaveFlashVideo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormWatch";
            this.Text = "Watch Video";
            ((System.ComponentModel.ISupportInitialize)(this.axShockwaveFlashVideo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxShockwaveFlashObjects.AxShockwaveFlash axShockwaveFlashVideo;
        private System.Windows.Forms.Button buttonWatch;
    }
}