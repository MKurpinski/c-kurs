﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MichalKurpinskiZad1
{
    [Serializable]
    public class Achievement
    {
        public string Achieve { get; set; }
        public int Year { get; set; }
        public Achievement() { }
        public Achievement(String ach, int year)
        {
            Achieve = ach;
            Year = year;
        }
    }
}
