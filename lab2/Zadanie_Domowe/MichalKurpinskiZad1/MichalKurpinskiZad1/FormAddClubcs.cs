﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiZad1
{
    public partial class FormAddClubcs : Form 
    {
        FormMain owner;//"wlaściciel" tej formy, pozwala nam na odwoływanie się do elementów naszej głownej formy.
        public string openFileName;//Nazwa herbu, który chcemy wczytać.
        public FormAddClubcs(FormMain owner)
        {
            InitializeComponent();
            this.owner = owner;
        }
        /// <summary>
        /// zamyka formę i anuluje dodawanie elementu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Po kliknięciu w TextBox usuwa jego domyślny tekst.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxName_Click(object sender, EventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            textBox.Text = "";
        }
        /// <summary>
        /// Wczytywanie herbu z pliku.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddIEmblemat_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "JPG Files (*.jpg)|*.jpg|JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|GIF Files (*.gif)|*.gif";
            openFileDialog.Title = "Open emblem of club from file...";
            openFileDialog.FilterIndex = 1;
            openFileDialog.ShowDialog();
            openFileName = openFileDialog.FileName;
        }
        /// <summary>
        /// Potwierdzenie dodania klubu, Tworzy instancję klub, dodaje do listy klubów i aktualizuje DataGridViewListOfPeople
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            try
            {

                owner.listOfClubs.Add(new Club(textBoxName.Text, textBoxLocalization.Text,textBoxStadium.Text, int.Parse(textBoxYear.Text), new Bitmap(openFileName)));
                owner.dataGridViewClubs.DataSource = null;
                owner.dataGridViewClubs.DataSource = owner.listOfClubs;
                owner.bravo.Play();
            }
            catch (Exception)
            {
                owner.error.Play();
                MessageBox.Show("Nieprawidłowe dane!", "Error");
             
            }
            finally { 
            this.Close();
            }
        }
    }
}
