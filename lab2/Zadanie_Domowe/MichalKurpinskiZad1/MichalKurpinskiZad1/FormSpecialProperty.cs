﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiZad1
{
    public partial class FormSpecialProperty : Form
    {
        FormMain owner;//"wlaściciel" tej formy, pozwala nam na odwoływanie się do elementów naszej głownej formy.
        public FormSpecialProperty(FormMain owner)
        {
            InitializeComponent();
            this.owner = owner;
        }
        /// <summary>
        /// zamyka formę i anuluje dodawanie własciwości.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Potwierdza dodanie własciwości do klubu. Tworzy instancję własciwości i dodaje do listy zaznaczonego klubu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                owner.listOfClubs[owner.dataGridViewClubs.SelectedRows[0].Index].properties.Add(new Property(textBoxProperty.Text));
                this.Close();
            }
            catch (Exception)
            {
                owner.error.Play();
                MessageBox.Show("Żaden klub nie został zaznaczony", "Error");
            }
            finally
            {
                this.Close();
            }
        }
        /// <summary>
        /// Kliknnięcie w TextBox usuwa z niego domyślny tekst.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxProperty_MouseClick(object sender, MouseEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            textBox.Text = "";
        }
    }
}
