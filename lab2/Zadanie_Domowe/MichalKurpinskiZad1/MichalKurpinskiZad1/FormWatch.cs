﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiZad1
{
    public partial class FormWatch : Form
    {
        string[] movies = new string[5];//Tablica linków do filmów.
        Random random = new Random();
        public FormWatch()
        {
            InitializeComponent();
            movies[0] = "https://www.youtube.com/v/-NSvbGxzpKk";
            movies[1] = "https://www.youtube.com/v/7iaWYNKknSY";
            movies[2] = "https://www.youtube.com/v/Gzewqj0yjoQ";
            movies[3] = "https://www.youtube.com/v/79waWaREyOY";
            movies[4] = "https://www.youtube.com/v/sDYSTWqYZg4";
        }
        /// <summary>
        /// Wylowanie jednego z 5 filmów i ustawienie go jako tego, który ma się wyświetlić po przyciśnięciu przycisku.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonWatch_Click(object sender, EventArgs e)
        {
            axShockwaveFlashVideo.Movie = movies[random.Next(4)];
        }
    }
}
