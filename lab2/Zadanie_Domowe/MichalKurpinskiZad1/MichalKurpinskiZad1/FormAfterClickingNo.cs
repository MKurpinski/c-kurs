﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiZad1
{
    public partial class FormAfterClickingNo : Form
    {/// <summary>
    /// Ustawianie w konstruktorze Obrazu dla PictureBoxa.
    /// </summary>
        public FormAfterClickingNo()
        {
            InitializeComponent();
            pictureBoxOK.Image = Properties.Resources.Guardiola;
        }
        /// <summary>
        /// zamykanie formy.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
