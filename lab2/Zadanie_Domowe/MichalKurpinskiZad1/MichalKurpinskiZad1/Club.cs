﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace MichalKurpinskiZad1
{
    [Serializable]
    public class Club 

    {
        public string Name { get; set; }
        public string Stadium { get; set; }
        public string Localization { get; set; }
        public int Year { get; set; }
        public List<Property> properties = new List<Property>();
        public List<Achievement> achieves = new List<Achievement>();
        private Bitmap emblem;
        public Club() { }
        public Club(string name,string localization,string stadium,int year,Bitmap emblem)
        {
            Name = name;
            Localization = localization;
            Stadium = stadium;
            Year = year;
            this.emblem = emblem;
        }
        /// <summary>
        /// nie pozwolenie na użwanie tego elementu przez dataGridView oraz ignorowanie tego elementu podczas zapisu do pliku xml.
        /// </summary>
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public Bitmap Emblem
        {
            get { return emblem; }
            set { emblem = value; }
        }
        /// <summary>
        /// Serializowanie obrazu(znalezione na internecie) oraz nie pozwolenie na użwanie tego elementu przez dataGridView.
        /// </summary>
        [Browsable(false)]
        [XmlElementAttribute("Picture")]
        public byte[] PictureByteArray
        {
            get
            {
                if (emblem != null)
                {
                    TypeConverter BitmapConverter =
                         TypeDescriptor.GetConverter(emblem.GetType());
                    return (byte[])
                         BitmapConverter.ConvertTo(emblem, typeof(byte[]));
                }
                else
                    return null;
            }

            set
            {
                if (value != null)
                    emblem = new Bitmap(new MemoryStream(value));
                else
                    emblem = null;
            }
        }
    }
    }
