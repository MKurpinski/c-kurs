﻿namespace MichalKurpinskiZad1
{
    partial class FormSpecialProperty
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSpecialProperty));
            this.labelAddSpecialPropert = new System.Windows.Forms.Label();
            this.buttonConfirm = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.textBoxProperty = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // labelAddSpecialPropert
            // 
            this.labelAddSpecialPropert.AutoSize = true;
            this.labelAddSpecialPropert.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.labelAddSpecialPropert.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAddSpecialPropert.Location = new System.Drawing.Point(201, 37);
            this.labelAddSpecialPropert.Name = "labelAddSpecialPropert";
            this.labelAddSpecialPropert.Size = new System.Drawing.Size(462, 55);
            this.labelAddSpecialPropert.TabIndex = 0;
            this.labelAddSpecialPropert.Text = "Add special property";
            this.labelAddSpecialPropert.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.Location = new System.Drawing.Point(31, 354);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(392, 98);
            this.buttonConfirm.TabIndex = 1;
            this.buttonConfirm.Text = "Confirm";
            this.buttonConfirm.UseVisualStyleBackColor = true;
            this.buttonConfirm.Click += new System.EventHandler(this.buttonConfirm_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(448, 354);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(392, 98);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // textBoxProperty
            // 
            this.textBoxProperty.Location = new System.Drawing.Point(31, 160);
            this.textBoxProperty.Multiline = true;
            this.textBoxProperty.Name = "textBoxProperty";
            this.textBoxProperty.Size = new System.Drawing.Size(809, 134);
            this.textBoxProperty.TabIndex = 3;
            this.textBoxProperty.Text = "Write here special property..";
            this.textBoxProperty.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBoxProperty_MouseClick);
            // 
            // SpecialProperty
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MichalKurpinskiZad1.Properties.Resources.trawa;
            this.ClientSize = new System.Drawing.Size(888, 507);
            this.Controls.Add(this.textBoxProperty);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonConfirm);
            this.Controls.Add(this.labelAddSpecialPropert);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SpecialProperty";
            this.Text = "Add special property";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelAddSpecialPropert;
        private System.Windows.Forms.Button buttonConfirm;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TextBox textBoxProperty;
    }
}