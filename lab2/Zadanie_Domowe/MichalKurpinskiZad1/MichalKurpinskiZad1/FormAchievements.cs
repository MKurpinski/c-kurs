﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiZad1
{
    public partial class FormAchievements : Form
    {
        FormMain owner;//"wlaściciel" tej formy, pozwala nam na odwoływanie się do elementów naszej głownej formy.
        public FormAchievements(FormMain owner)
        {
            InitializeComponent();
            this.owner = owner;
        }
        /// <summary>
        /// Przy kliknięciu usuwa z TextBoxów domyślny tekst i pozwala wpisać swój bez koniecznosci recznego usuwania znaków.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxAchieve_MouseClick(object sender, MouseEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            textBox.Text = "";
        }
        /// <summary>
        /// Zamyka okno i anuluje dodawnie osiągnięcia.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// potwierdzenie dodania osiagnięcia do klubu. Tworzy nową instancję Osiągniecie i dodaje do listy osiagnieć danego klubu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                owner.listOfClubs[owner.dataGridViewClubs.SelectedRows[0].Index].achieves.Add(new Achievement(textBoxAchieve.Text, int.Parse(textBoxYear.Text)));
                owner.bravo.Play();
                this.Close();
            }
            catch (Exception )
            {
                owner.error.Play();
                MessageBox.Show("Nieprawidłowe dane!", "Error");
                
            }
        }

    }
}
