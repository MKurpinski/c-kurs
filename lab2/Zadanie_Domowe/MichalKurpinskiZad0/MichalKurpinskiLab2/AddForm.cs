﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace MichalKurpinskiLab2
{
    public partial class AddForm : Form
    {
        FormMain owner;//"własciciel" tej formy.
        public AddForm(FormMain owner)
        {
            InitializeComponent();
            this.owner = owner;
        }
            /// <summary>
            /// Wyjscie z okna
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Dodanie do listy osoby o wpisanych do TextBoxów parametrach.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                owner.listOfPeople.Add(new Person(textBoxSurname.Text, textBoxName.Text, int.Parse(textBoxAge.Text)));
                owner.dataGridViewListOfPeople.DataSource = null;
                owner.dataGridViewListOfPeople.DataSource = owner.listOfPeople;
                this.Close();
            }
            catch (Exception) {
                owner.error.Play();
                MessageBox.Show("Złe dane wejsciowe!", "Error");
                this.Close();
            }
        }
        /// <summary>
        /// Po kliknieciu myszką znika tekst w którymkolwiek z TextBoxów.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_MouseClick(object sender, MouseEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            textBox.Text = "";

        }
    }
}
