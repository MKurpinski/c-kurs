﻿namespace MichalKurpinskiLab2
{
    partial class AddGrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddGrade));
            this.textBoxGrade = new System.Windows.Forms.TextBox();
            this.textBoxNameOfGrade = new System.Windows.Forms.TextBox();
            this.buttonConfirm = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelTitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxGrade
            // 
            this.textBoxGrade.Location = new System.Drawing.Point(206, 124);
            this.textBoxGrade.Multiline = true;
            this.textBoxGrade.Name = "textBoxGrade";
            this.textBoxGrade.Size = new System.Drawing.Size(361, 46);
            this.textBoxGrade.TabIndex = 0;
            this.textBoxGrade.Text = "Grade";
            this.textBoxGrade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxGrade.Click += new System.EventHandler(this.textBoxGrade_Click);
            // 
            // textBoxNameOfGrade
            // 
            this.textBoxNameOfGrade.Location = new System.Drawing.Point(206, 209);
            this.textBoxNameOfGrade.Multiline = true;
            this.textBoxNameOfGrade.Name = "textBoxNameOfGrade";
            this.textBoxNameOfGrade.Size = new System.Drawing.Size(361, 46);
            this.textBoxNameOfGrade.TabIndex = 1;
            this.textBoxNameOfGrade.Text = "Name of Grade";
            this.textBoxNameOfGrade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxNameOfGrade.Click += new System.EventHandler(this.textBoxGrade_Click);
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.BackColor = System.Drawing.Color.SeaShell;
            this.buttonConfirm.Location = new System.Drawing.Point(23, 308);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(339, 69);
            this.buttonConfirm.TabIndex = 2;
            this.buttonConfirm.Text = "Confirm";
            this.buttonConfirm.UseVisualStyleBackColor = false;
            this.buttonConfirm.Click += new System.EventHandler(this.buttonConfirm_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.SeaShell;
            this.buttonCancel.Location = new System.Drawing.Point(416, 308);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(339, 69);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTitle.Location = new System.Drawing.Point(222, 27);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(325, 52);
            this.labelTitle.TabIndex = 4;
            this.labelTitle.Text = "Add new grade";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AddGrade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PeachPuff;
            this.ClientSize = new System.Drawing.Size(767, 417);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonConfirm);
            this.Controls.Add(this.textBoxNameOfGrade);
            this.Controls.Add(this.textBoxGrade);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AddGrade";
            this.Text = "AddGrade";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxGrade;
        private System.Windows.Forms.TextBox textBoxNameOfGrade;
        private System.Windows.Forms.Button buttonConfirm;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelTitle;
    }
}