﻿namespace MichalKurpinskiLab2
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.labelName = new System.Windows.Forms.Label();
            this.dataGridViewListOfPeople = new System.Windows.Forms.DataGridView();
            this.buttonAddPerson = new System.Windows.Forms.Button();
            this.buttonShow = new System.Windows.Forms.Button();
            this.labelListOfPeople = new System.Windows.Forms.Label();
            this.labelGrades = new System.Windows.Forms.Label();
            this.dataGridViewGrades = new System.Windows.Forms.DataGridView();
            this.buttonAddGrade = new System.Windows.Forms.Button();
            this.buttonShowGrades = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonOpen = new System.Windows.Forms.Button();
            this.pictureBoxSuprise = new System.Windows.Forms.PictureBox();
            this.pictureBoxSmile = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListOfPeople)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSuprise)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSmile)).BeginInit();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.Location = new System.Drawing.Point(953, 36);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(697, 102);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Michał Kurpiński";
            // 
            // dataGridViewListOfPeople
            // 
            this.dataGridViewListOfPeople.AllowUserToResizeColumns = false;
            this.dataGridViewListOfPeople.AllowUserToResizeRows = false;
            this.dataGridViewListOfPeople.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewListOfPeople.BackgroundColor = System.Drawing.Color.Salmon;
            this.dataGridViewListOfPeople.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListOfPeople.Location = new System.Drawing.Point(414, 233);
            this.dataGridViewListOfPeople.Name = "dataGridViewListOfPeople";
            this.dataGridViewListOfPeople.RowTemplate.Height = 37;
            this.dataGridViewListOfPeople.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewListOfPeople.Size = new System.Drawing.Size(880, 882);
            this.dataGridViewListOfPeople.TabIndex = 1;
            // 
            // buttonAddPerson
            // 
            this.buttonAddPerson.BackColor = System.Drawing.Color.SeaShell;
            this.buttonAddPerson.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddPerson.Location = new System.Drawing.Point(25, 405);
            this.buttonAddPerson.Name = "buttonAddPerson";
            this.buttonAddPerson.Size = new System.Drawing.Size(341, 96);
            this.buttonAddPerson.TabIndex = 5;
            this.buttonAddPerson.Text = "Add new person";
            this.buttonAddPerson.UseVisualStyleBackColor = false;
            this.buttonAddPerson.Click += new System.EventHandler(this.buttonAddPerson_Click);
            // 
            // buttonShow
            // 
            this.buttonShow.BackColor = System.Drawing.Color.SeaShell;
            this.buttonShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonShow.Location = new System.Drawing.Point(25, 233);
            this.buttonShow.Name = "buttonShow";
            this.buttonShow.Size = new System.Drawing.Size(339, 102);
            this.buttonShow.TabIndex = 6;
            this.buttonShow.Text = "Show list of people";
            this.buttonShow.UseVisualStyleBackColor = false;
            this.buttonShow.Click += new System.EventHandler(this.buttonShow_Click);
            // 
            // labelListOfPeople
            // 
            this.labelListOfPeople.AutoSize = true;
            this.labelListOfPeople.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelListOfPeople.Location = new System.Drawing.Point(662, 163);
            this.labelListOfPeople.Name = "labelListOfPeople";
            this.labelListOfPeople.Size = new System.Drawing.Size(314, 55);
            this.labelListOfPeople.TabIndex = 7;
            this.labelListOfPeople.Text = "List of People";
            // 
            // labelGrades
            // 
            this.labelGrades.AutoSize = true;
            this.labelGrades.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelGrades.Location = new System.Drawing.Point(1719, 163);
            this.labelGrades.Name = "labelGrades";
            this.labelGrades.Size = new System.Drawing.Size(182, 55);
            this.labelGrades.TabIndex = 8;
            this.labelGrades.Text = "Grades";
            // 
            // dataGridViewGrades
            // 
            this.dataGridViewGrades.AllowUserToResizeColumns = false;
            this.dataGridViewGrades.AllowUserToResizeRows = false;
            this.dataGridViewGrades.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewGrades.BackgroundColor = System.Drawing.Color.Salmon;
            this.dataGridViewGrades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewGrades.Location = new System.Drawing.Point(1365, 233);
            this.dataGridViewGrades.Name = "dataGridViewGrades";
            this.dataGridViewGrades.RowTemplate.Height = 37;
            this.dataGridViewGrades.Size = new System.Drawing.Size(920, 882);
            this.dataGridViewGrades.TabIndex = 9;
            // 
            // buttonAddGrade
            // 
            this.buttonAddGrade.BackColor = System.Drawing.Color.SeaShell;
            this.buttonAddGrade.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddGrade.Location = new System.Drawing.Point(27, 717);
            this.buttonAddGrade.Name = "buttonAddGrade";
            this.buttonAddGrade.Size = new System.Drawing.Size(339, 103);
            this.buttonAddGrade.TabIndex = 10;
            this.buttonAddGrade.Text = "Add grade";
            this.buttonAddGrade.UseVisualStyleBackColor = false;
            this.buttonAddGrade.Click += new System.EventHandler(this.buttonAddGrade_Click);
            // 
            // buttonShowGrades
            // 
            this.buttonShowGrades.BackColor = System.Drawing.Color.SeaShell;
            this.buttonShowGrades.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonShowGrades.Location = new System.Drawing.Point(27, 557);
            this.buttonShowGrades.Name = "buttonShowGrades";
            this.buttonShowGrades.Size = new System.Drawing.Size(339, 109);
            this.buttonShowGrades.TabIndex = 11;
            this.buttonShowGrades.Text = "Print grades of person";
            this.buttonShowGrades.UseVisualStyleBackColor = false;
            this.buttonShowGrades.Click += new System.EventHandler(this.buttonShowGrades_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.BackColor = System.Drawing.Color.SeaShell;
            this.buttonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSave.Location = new System.Drawing.Point(27, 858);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(339, 103);
            this.buttonSave.TabIndex = 15;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonOpen
            // 
            this.buttonOpen.BackColor = System.Drawing.Color.SeaShell;
            this.buttonOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonOpen.Location = new System.Drawing.Point(25, 1012);
            this.buttonOpen.Name = "buttonOpen";
            this.buttonOpen.Size = new System.Drawing.Size(339, 103);
            this.buttonOpen.TabIndex = 16;
            this.buttonOpen.Text = "Open";
            this.buttonOpen.UseVisualStyleBackColor = false;
            this.buttonOpen.Click += new System.EventHandler(this.buttonOpen_Click);
            // 
            // pictureBoxSuprise
            // 
            this.pictureBoxSuprise.BackColor = System.Drawing.Color.Salmon;
            this.pictureBoxSuprise.Location = new System.Drawing.Point(1454, 530);
            this.pictureBoxSuprise.Name = "pictureBoxSuprise";
            this.pictureBoxSuprise.Size = new System.Drawing.Size(831, 538);
            this.pictureBoxSuprise.TabIndex = 17;
            this.pictureBoxSuprise.TabStop = false;
            // 
            // pictureBoxSmile
            // 
            this.pictureBoxSmile.Location = new System.Drawing.Point(2300, 73);
            this.pictureBoxSmile.Name = "pictureBoxSmile";
            this.pictureBoxSmile.Size = new System.Drawing.Size(248, 205);
            this.pictureBoxSmile.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSmile.TabIndex = 14;
            this.pictureBoxSmile.TabStop = false;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(216F, 216F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.PeachPuff;
            this.ClientSize = new System.Drawing.Size(2608, 1173);
            this.Controls.Add(this.pictureBoxSuprise);
            this.Controls.Add(this.buttonOpen);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.pictureBoxSmile);
            this.Controls.Add(this.buttonShowGrades);
            this.Controls.Add(this.buttonAddGrade);
            this.Controls.Add(this.dataGridViewGrades);
            this.Controls.Add(this.labelGrades);
            this.Controls.Add(this.labelListOfPeople);
            this.Controls.Add(this.buttonShow);
            this.Controls.Add(this.buttonAddPerson);
            this.Controls.Add(this.dataGridViewListOfPeople);
            this.Controls.Add(this.labelName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "Spis Uczniów";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListOfPeople)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGrades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSuprise)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSmile)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Button buttonAddPerson;
        private System.Windows.Forms.Button buttonShow;
        private System.Windows.Forms.Label labelListOfPeople;
        private System.Windows.Forms.Label labelGrades;
        private System.Windows.Forms.Button buttonAddGrade;
        private System.Windows.Forms.PictureBox pictureBoxSmile;
        public System.Windows.Forms.DataGridView dataGridViewGrades;
        public System.Windows.Forms.DataGridView dataGridViewListOfPeople;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonOpen;
        private System.Windows.Forms.PictureBox pictureBoxSuprise;
        public System.Windows.Forms.Button buttonShowGrades;
    }
}

