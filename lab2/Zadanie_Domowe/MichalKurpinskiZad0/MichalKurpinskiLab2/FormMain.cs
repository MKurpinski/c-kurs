﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using System.Xml.Serialization;
using System.IO;

namespace MichalKurpinskiLab2
{
    public partial class FormMain : Form
    {
        XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Person>));
        public SystemSound error = SystemSounds.Hand;
        SoundPlayer goodStudent = new SoundPlayer(Properties.Resources.Fanfary);
        public List<Person> listOfPeople; 
        public FormMain()
        {
            InitializeComponent();
            Person person = new Person("Michał","Kurpiński",20);
            listOfPeople=new List<Person>();
        }
        /// <summary>
        /// Przycisk otwierający okno, które umożliwia nam dodanie osoby do listy.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddPerson_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm(this);
            addForm.ShowDialog();

        }
        /// <summary>
        /// Wyświetlenie listy osob.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonShow_Click(object sender, EventArgs e)
        {
            dataGridViewListOfPeople.DataSource = null;
            dataGridViewListOfPeople.DataSource = listOfPeople;
        }
        /// <summary>
        /// Wyświetlenie ocen odpowiedniej osoby
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void buttonShowGrades_Click(object sender, EventArgs e)
        {
            pictureBoxSuprise.Image = null;
            try
            {
                dataGridViewGrades.DataSource = null;
                dataGridViewGrades.DataSource = listOfPeople[dataGridViewListOfPeople.SelectedRows[0].Index].listOfGrades;
                pictureBoxSmile.Image = null;
                    for(int i = 0;i< listOfPeople[dataGridViewListOfPeople.SelectedRows[0].Index].listOfGrades.Count; i++)
                {
                   if (listOfPeople[dataGridViewListOfPeople.SelectedRows[0].Index].listOfGrades[i].Value < 3)
                    {
                        pictureBoxSmile.Image = Properties.Resources.sad;
                    }
                }
                    if(pictureBoxSmile.Image == null){
                    pictureBoxSmile.Image = Properties.Resources.smile;
                }
                int average = 0; ; // średnia studenta
                foreach (Grade g in listOfPeople[dataGridViewListOfPeople.SelectedRows[0].Index].listOfGrades) {
                    average += g.Value;
                }
                if((average/ listOfPeople[dataGridViewListOfPeople.SelectedRows[0].Index].listOfGrades.Count) >= 4)
                {
                    goodStudent.Play();
                    pictureBoxSuprise.Image = Properties.Resources.Obama;
                    MessageBox.Show("Wow,Wow! Jesteś takim dobrym studentem!");
                }
            }
            catch (Exception ) {
                error.Play();
                MessageBox.Show("Nie zaznaczyłes żadnego wiersza! Zaznacz jakiś wiersz użytkowniku:))");
            }
        }
        /// <summary>
        /// Przycisk otwierający okno, które umożliwia nam dodanie oceny do osoby.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddGrade_Click(object sender, EventArgs e)
        {
            AddGrade addGrade = new AddGrade(this);
            addGrade.ShowDialog();
        }
        /// <summary>
        /// zapisywanie naszej listy do pliku, który sami wybierzemy.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSave_Click(object sender, EventArgs e)
        {

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "XMLFiles(*.xml)\"|*.xml";
            saveFileDialog.AddExtension = true;
            saveFileDialog.ShowDialog();
            string saveFileName= saveFileDialog.FileName;
            try
            {
                StreamWriter writer = new StreamWriter(saveFileName);
                xmlSerializer.Serialize(writer, listOfPeople);
                writer.Flush();
                writer.Close();
                writer.Dispose();
            }
            catch (Exception) {
            }
            
        }
        /// <summary>
        /// Otwieranie listy z zapisanego pliku.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "XMLFiles(*.xml)\"|*.xml";
            openFileDialog.FilterIndex = 1;
            openFileDialog.ShowDialog();
            string openFileName = openFileDialog.FileName;
            try
            {
                StreamReader reader = new StreamReader(openFileName);
                listOfPeople = (List<Person>)xmlSerializer.Deserialize(reader);
                dataGridViewListOfPeople.DataSource = null;
                dataGridViewListOfPeople.DataSource = listOfPeople;
                reader.Close();
            }
            catch (Exception) { }

        }

    }
}
