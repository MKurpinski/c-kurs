﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MichalKurpinskiLab2
{
    [Serializable]//Dzięki temu możemy serializować nasze obiekty, a co za tym idzie, łatwo zapisać je do pliku.
    public class Person
    {
            public List<Grade> listOfGrades = new List<Grade>();
            public string Surname { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }
        public Person(string surname, string name, int age) {
            this.Surname = surname;
            this.Name = name;
            this.Age = age;
        }
        public Person() { }
    }
}
