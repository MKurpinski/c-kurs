﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiLab2
{
    public partial class AddGrade : Form
    {
        FormMain owner;//"własciciel" tej formy.
        public AddGrade(FormMain owner)
        {
            InitializeComponent();
            this.owner = owner; 
        }

        /// <summary>
        /// Wyjscie z okna.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Potwerdzenie dodania oceny o parametrach wpisanych do TextBoxow do zaznaczonej osoby.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                owner.listOfPeople[owner.dataGridViewListOfPeople.SelectedRows[0].Index].listOfGrades.Add(new Grade(int.Parse(textBoxGrade.Text), textBoxNameOfGrade.Text));
                //owner.dataGridViewGrades.DataSource = null;
                // owner.dataGridViewGrades.DataSource = owner.listOfPeople[owner.dataGridViewListOfPeople.SelectedRows[0].Index].listOfGrades;
                owner.buttonShowGrades_Click(owner.buttonShowGrades, e);
                this.Close();
            }
            catch (Exception)
            {
                owner.error.Play();
                MessageBox.Show("Złe dane wejściowe lub nie zaznaczony żaden człowiek", "Error");
                this.Close();
            }
        }
        //Przy nacisnieciu na którykolwiek TextBox znika na nim napis.
        private void textBoxGrade_Click(object sender, EventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            textBox.Text = "";
        }
    }
}
