﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MichalKurpinskiLab2
{   
    class SuperHuman : Person
    {
        public int AmountOfHands { get; set; }
        public SuperHuman(string surname, string name, int age):base(surname,name,age) {
            
        }
        public SuperHuman(string surname, string name, int age, int amountOfHands) : base(surname, name, age) {
            this.AmountOfHands = amountOfHands;
        }
        public int GetAmountOfHands() {
            return AmountOfHands;
        }
    }
}
