﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiLab2
{
    public partial class FormMain : Form
    {
        List<Person> listOfPeople;
        public FormMain()
        {
            InitializeComponent();
            Person person = new Person("Michał","Kurpiński",20);
            //   person.name = "Michał";
            //   person.surname = "Kurpiński";
            //   person.age = 21;
            MessageBox.Show(person.Name + " " + person.Surname);
            //   SuperHuman superHuman = new SuperHuman("Super", "Human", 25);
            SuperHuman superHuman = new SuperHuman("Super", "Human", 25,10);
           // superHuman.amountOfHands = 10;
            MessageBox.Show(superHuman.Name + superHuman.GetAmountOfHands().ToString());
            listOfPeople = new List<Person>();
            listOfPeople.Add(new Person("Michał", "Kurpiński", 20));
            listOfPeople.Add(new Person("Justyna", "Setlak", 20));
            listOfPeople.Add(new Person("Jan", "Kowalski", 25));
            listOfPeople.Add(new Person("Maciek", "Nowak", 30));
            listOfPeople.Add(new Person("Tomek", "Mikulski", 26));
            listOfPeople.Add(new Person("Andrzej", "Zieliński", 20));
            

        }
        
        private void buttonAddPerson_Click(object sender, EventArgs e)
        {
            listOfPeople.Add(new Person(textBoxSurname.Text, textBoxName.Text, int.Parse(textBoxAge.Text)));
        }

        private void buttonShow_Click(object sender, EventArgs e)
        {
            dataGridViewListOfPeople.DataSource = null;
            dataGridViewListOfPeople.DataSource = listOfPeople;
        }

        private void buttonShowGrades_Click(object sender, EventArgs e)
        {
            // if (dataGridViewListOfPeople.SelectedRows.Count > 0)
            // {
            //     dataGridViewGrades.DataSource = listOfPeople[dataGridViewListOfPeople.SelectedRows[0].Index].listOfGrades;
            // }
            // else {
            //     MessageBox.Show("Nie zaznaczyłes żadnego wiersza!");
            //}
            try
            {
                dataGridViewGrades.DataSource = null;
                dataGridViewGrades.DataSource = listOfPeople[dataGridViewListOfPeople.SelectedRows[0].Index].listOfGrades;
                pictureBoxSmile.BackgroundImage = null;
                    for(int i = 0;i< listOfPeople[dataGridViewListOfPeople.SelectedRows[0].Index].listOfGrades.Count; i++)
                {
                   if (listOfPeople[dataGridViewListOfPeople.SelectedRows[0].Index].listOfGrades[i].Value < 3)
                    {
                        pictureBoxSmile.BackgroundImage = Properties.Resources._07fae544d1885162med;
                    }
                }
                    if(pictureBoxSmile.BackgroundImage == null){
                    pictureBoxSmile.BackgroundImage = Properties.Resources.images;
                }
            }
            catch (Exception ) {
                MessageBox.Show("Nie zaznaczyłes żadnego wiersza! Zaznacz jakiś wiersz użytkowniku:))");
            }
        }

        private void buttonAddGrade_Click(object sender, EventArgs e)
        {
            listOfPeople[dataGridViewListOfPeople.SelectedRows[0].Index].listOfGrades.Add(new Grade(int.Parse(textBoxGrade.Text), textBoxNameOfGrade.Text));
        }
    }
}
