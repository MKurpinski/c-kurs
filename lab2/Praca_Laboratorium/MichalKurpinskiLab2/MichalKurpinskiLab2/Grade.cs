﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MichalKurpinskiLab2
{
    class Grade
    {
        public int Value { get; set; }
        public string NameOfGrade { get; set; }
        public Grade(int value, string nameOfGrade) {
            Value = value;
            NameOfGrade = nameOfGrade;
        }
    }
}
