﻿namespace MichalKurpinskiLab2
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelName = new System.Windows.Forms.Label();
            this.dataGridViewListOfPeople = new System.Windows.Forms.DataGridView();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxSurname = new System.Windows.Forms.TextBox();
            this.textBoxAge = new System.Windows.Forms.TextBox();
            this.buttonAddPerson = new System.Windows.Forms.Button();
            this.buttonShow = new System.Windows.Forms.Button();
            this.labelListOfPeople = new System.Windows.Forms.Label();
            this.labelGrades = new System.Windows.Forms.Label();
            this.dataGridViewGrades = new System.Windows.Forms.DataGridView();
            this.buttonAddGrade = new System.Windows.Forms.Button();
            this.buttonShowGrades = new System.Windows.Forms.Button();
            this.textBoxGrade = new System.Windows.Forms.TextBox();
            this.textBoxNameOfGrade = new System.Windows.Forms.TextBox();
            this.pictureBoxSmile = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListOfPeople)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSmile)).BeginInit();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.Location = new System.Drawing.Point(953, 36);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(697, 102);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Michał Kurpiński";
            // 
            // dataGridViewListOfPeople
            // 
            this.dataGridViewListOfPeople.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListOfPeople.Location = new System.Drawing.Point(402, 233);
            this.dataGridViewListOfPeople.Name = "dataGridViewListOfPeople";
            this.dataGridViewListOfPeople.RowTemplate.Height = 37;
            this.dataGridViewListOfPeople.Size = new System.Drawing.Size(880, 774);
            this.dataGridViewListOfPeople.TabIndex = 1;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(204, 233);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(169, 35);
            this.textBoxName.TabIndex = 2;
            this.textBoxName.Text = "Imię";
            // 
            // textBoxSurname
            // 
            this.textBoxSurname.Location = new System.Drawing.Point(204, 335);
            this.textBoxSurname.Name = "textBoxSurname";
            this.textBoxSurname.Size = new System.Drawing.Size(169, 35);
            this.textBoxSurname.TabIndex = 3;
            this.textBoxSurname.Text = "Nazwisko";
            // 
            // textBoxAge
            // 
            this.textBoxAge.Location = new System.Drawing.Point(204, 426);
            this.textBoxAge.Name = "textBoxAge";
            this.textBoxAge.Size = new System.Drawing.Size(169, 35);
            this.textBoxAge.TabIndex = 4;
            this.textBoxAge.Text = "Wiek";
            // 
            // buttonAddPerson
            // 
            this.buttonAddPerson.Location = new System.Drawing.Point(23, 378);
            this.buttonAddPerson.Name = "buttonAddPerson";
            this.buttonAddPerson.Size = new System.Drawing.Size(135, 66);
            this.buttonAddPerson.TabIndex = 5;
            this.buttonAddPerson.Text = "Add";
            this.buttonAddPerson.UseVisualStyleBackColor = true;
            this.buttonAddPerson.Click += new System.EventHandler(this.buttonAddPerson_Click);
            // 
            // buttonShow
            // 
            this.buttonShow.Location = new System.Drawing.Point(25, 278);
            this.buttonShow.Name = "buttonShow";
            this.buttonShow.Size = new System.Drawing.Size(135, 61);
            this.buttonShow.TabIndex = 6;
            this.buttonShow.Text = "Show";
            this.buttonShow.UseVisualStyleBackColor = true;
            this.buttonShow.Click += new System.EventHandler(this.buttonShow_Click);
            // 
            // labelListOfPeople
            // 
            this.labelListOfPeople.AutoSize = true;
            this.labelListOfPeople.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelListOfPeople.Location = new System.Drawing.Point(662, 163);
            this.labelListOfPeople.Name = "labelListOfPeople";
            this.labelListOfPeople.Size = new System.Drawing.Size(314, 55);
            this.labelListOfPeople.TabIndex = 7;
            this.labelListOfPeople.Text = "List of People";
            // 
            // labelGrades
            // 
            this.labelGrades.AutoSize = true;
            this.labelGrades.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelGrades.Location = new System.Drawing.Point(1519, 163);
            this.labelGrades.Name = "labelGrades";
            this.labelGrades.Size = new System.Drawing.Size(182, 55);
            this.labelGrades.TabIndex = 8;
            this.labelGrades.Text = "Grades";
            // 
            // dataGridViewGrades
            // 
            this.dataGridViewGrades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewGrades.Location = new System.Drawing.Point(1365, 233);
            this.dataGridViewGrades.Name = "dataGridViewGrades";
            this.dataGridViewGrades.RowTemplate.Height = 37;
            this.dataGridViewGrades.Size = new System.Drawing.Size(920, 774);
            this.dataGridViewGrades.TabIndex = 9;
            // 
            // buttonAddGrade
            // 
            this.buttonAddGrade.Location = new System.Drawing.Point(23, 732);
            this.buttonAddGrade.Name = "buttonAddGrade";
            this.buttonAddGrade.Size = new System.Drawing.Size(135, 103);
            this.buttonAddGrade.TabIndex = 10;
            this.buttonAddGrade.Text = "Add grade";
            this.buttonAddGrade.UseVisualStyleBackColor = true;
            this.buttonAddGrade.Click += new System.EventHandler(this.buttonAddGrade_Click);
            // 
            // buttonShowGrades
            // 
            this.buttonShowGrades.Location = new System.Drawing.Point(23, 598);
            this.buttonShowGrades.Name = "buttonShowGrades";
            this.buttonShowGrades.Size = new System.Drawing.Size(137, 99);
            this.buttonShowGrades.TabIndex = 11;
            this.buttonShowGrades.Text = "Print Grades";
            this.buttonShowGrades.UseVisualStyleBackColor = true;
            this.buttonShowGrades.Click += new System.EventHandler(this.buttonShowGrades_Click);
            // 
            // textBoxGrade
            // 
            this.textBoxGrade.Location = new System.Drawing.Point(204, 662);
            this.textBoxGrade.Name = "textBoxGrade";
            this.textBoxGrade.Size = new System.Drawing.Size(169, 35);
            this.textBoxGrade.TabIndex = 12;
            this.textBoxGrade.Text = "Ocena";
            // 
            // textBoxNameOfGrade
            // 
            this.textBoxNameOfGrade.Location = new System.Drawing.Point(204, 732);
            this.textBoxNameOfGrade.Name = "textBoxNameOfGrade";
            this.textBoxNameOfGrade.Size = new System.Drawing.Size(169, 35);
            this.textBoxNameOfGrade.TabIndex = 13;
            this.textBoxNameOfGrade.Text = "Ocena(nazwa)";
            // 
            // pictureBoxSmile
            // 
            this.pictureBoxSmile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxSmile.Location = new System.Drawing.Point(2300, 73);
            this.pictureBoxSmile.Name = "pictureBoxSmile";
            this.pictureBoxSmile.Size = new System.Drawing.Size(186, 92);
            this.pictureBoxSmile.TabIndex = 14;
            this.pictureBoxSmile.TabStop = false;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2608, 1173);
            this.Controls.Add(this.pictureBoxSmile);
            this.Controls.Add(this.textBoxNameOfGrade);
            this.Controls.Add(this.textBoxGrade);
            this.Controls.Add(this.buttonShowGrades);
            this.Controls.Add(this.buttonAddGrade);
            this.Controls.Add(this.dataGridViewGrades);
            this.Controls.Add(this.labelGrades);
            this.Controls.Add(this.labelListOfPeople);
            this.Controls.Add(this.buttonShow);
            this.Controls.Add(this.buttonAddPerson);
            this.Controls.Add(this.textBoxAge);
            this.Controls.Add(this.textBoxSurname);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.dataGridViewListOfPeople);
            this.Controls.Add(this.labelName);
            this.Name = "FormMain";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListOfPeople)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGrades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSmile)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.DataGridView dataGridViewListOfPeople;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxSurname;
        private System.Windows.Forms.TextBox textBoxAge;
        private System.Windows.Forms.Button buttonAddPerson;
        private System.Windows.Forms.Button buttonShow;
        private System.Windows.Forms.Label labelListOfPeople;
        private System.Windows.Forms.Label labelGrades;
        private System.Windows.Forms.DataGridView dataGridViewGrades;
        private System.Windows.Forms.Button buttonAddGrade;
        private System.Windows.Forms.Button buttonShowGrades;
        private System.Windows.Forms.TextBox textBoxGrade;
        private System.Windows.Forms.TextBox textBoxNameOfGrade;
        private System.Windows.Forms.PictureBox pictureBoxSmile;
    }
}

