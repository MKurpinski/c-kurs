USE [master]
GO
/****** Object:  Database [MichalKurpinskiLab3]    Script Date: 26.04.2016 12:46:58 ******/
CREATE DATABASE [MichalKurpinskiLab3]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MichalKurpinskiLab3', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLSERVER\MSSQL\DATA\MichalKurpinskiLab3.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'MichalKurpinskiLab3_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLSERVER\MSSQL\DATA\MichalKurpinskiLab3_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [MichalKurpinskiLab3] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MichalKurpinskiLab3].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MichalKurpinskiLab3] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET ARITHABORT OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET  MULTI_USER 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MichalKurpinskiLab3] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [MichalKurpinskiLab3] SET DELAYED_DURABILITY = DISABLED 
GO
USE [MichalKurpinskiLab3]
GO
/****** Object:  Table [dbo].[Clients]    Script Date: 26.04.2016 12:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clients](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](50) NOT NULL,
	[ComputerID] [int] NOT NULL,
 CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_Computers]    Script Date: 26.04.2016 12:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_Computers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Producent] [nvarchar](50) NOT NULL,
	[Model] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Table_Computers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_Employees]    Script Date: 26.04.2016 12:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_Employees](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](50) NOT NULL,
	[Age] [int] NOT NULL,
 CONSTRAINT [PK_Table_Workers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_Services]    Script Date: 26.04.2016 12:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_Services](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ComputerID] [int] NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[Cost] [int] NOT NULL,
	[Descripction] [nvarchar](99) NOT NULL,
 CONSTRAINT [PK_Table_Services] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Clients] ON 

INSERT [dbo].[Clients] ([ID], [Name], [Surname], [ComputerID]) VALUES (1, N'Jan', N'Nowak', 1)
INSERT [dbo].[Clients] ([ID], [Name], [Surname], [ComputerID]) VALUES (2, N'Tomasz', N'Kowalski', 3)
INSERT [dbo].[Clients] ([ID], [Name], [Surname], [ComputerID]) VALUES (3, N'Maciej', N'Krawiec', 2)
SET IDENTITY_INSERT [dbo].[Clients] OFF
SET IDENTITY_INSERT [dbo].[Table_Computers] ON 

INSERT [dbo].[Table_Computers] ([ID], [Producent], [Model]) VALUES (1, N'ASUS', N'NV-500')
INSERT [dbo].[Table_Computers] ([ID], [Producent], [Model]) VALUES (2, N'Lenovo', N'AP-300')
INSERT [dbo].[Table_Computers] ([ID], [Producent], [Model]) VALUES (3, N'MSI', N'GS70')
SET IDENTITY_INSERT [dbo].[Table_Computers] OFF
SET IDENTITY_INSERT [dbo].[Table_Employees] ON 

INSERT [dbo].[Table_Employees] ([ID], [Name], [Surname], [Age]) VALUES (1, N'Michał', N'Kurpinski', 20)
INSERT [dbo].[Table_Employees] ([ID], [Name], [Surname], [Age]) VALUES (2, N'Artur', N'Jordan', 40)
INSERT [dbo].[Table_Employees] ([ID], [Name], [Surname], [Age]) VALUES (3, N'Justyna', N'Setlak', 20)
INSERT [dbo].[Table_Employees] ([ID], [Name], [Surname], [Age]) VALUES (4, N'Jan', N'Nowak', 45)
SET IDENTITY_INSERT [dbo].[Table_Employees] OFF
SET IDENTITY_INSERT [dbo].[Table_Services] ON 

INSERT [dbo].[Table_Services] ([ID], [ComputerID], [EmployeeID], [Cost], [Descripction]) VALUES (1, 1, 1, 500, N'Dysk')
INSERT [dbo].[Table_Services] ([ID], [ComputerID], [EmployeeID], [Cost], [Descripction]) VALUES (2, 1, 2, 600, N'RAM')
INSERT [dbo].[Table_Services] ([ID], [ComputerID], [EmployeeID], [Cost], [Descripction]) VALUES (3, 2, 3, 100, N'Help Desk')
INSERT [dbo].[Table_Services] ([ID], [ComputerID], [EmployeeID], [Cost], [Descripction]) VALUES (4, 3, 1, 200, N'Karta Graficzna')
INSERT [dbo].[Table_Services] ([ID], [ComputerID], [EmployeeID], [Cost], [Descripction]) VALUES (5, 3, 4, 100, N'Stacja Dysków')
SET IDENTITY_INSERT [dbo].[Table_Services] OFF
ALTER TABLE [dbo].[Clients]  WITH CHECK ADD  CONSTRAINT [FK_Clients_Table_Computers] FOREIGN KEY([ComputerID])
REFERENCES [dbo].[Table_Computers] ([ID])
GO
ALTER TABLE [dbo].[Clients] CHECK CONSTRAINT [FK_Clients_Table_Computers]
GO
ALTER TABLE [dbo].[Table_Services]  WITH CHECK ADD  CONSTRAINT [FK_Table_Services_Table_Computers] FOREIGN KEY([ComputerID])
REFERENCES [dbo].[Table_Computers] ([ID])
GO
ALTER TABLE [dbo].[Table_Services] CHECK CONSTRAINT [FK_Table_Services_Table_Computers]
GO
ALTER TABLE [dbo].[Table_Services]  WITH CHECK ADD  CONSTRAINT [FK_Table_Services_Table_Employees] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Table_Employees] ([ID])
GO
ALTER TABLE [dbo].[Table_Services] CHECK CONSTRAINT [FK_Table_Services_Table_Employees]
GO
USE [master]
GO
ALTER DATABASE [MichalKurpinskiLab3] SET  READ_WRITE 
GO
