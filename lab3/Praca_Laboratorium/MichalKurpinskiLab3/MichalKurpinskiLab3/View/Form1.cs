﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MichalKurpinskiLab3.Model;

namespace MichalKurpinskiLab3
{
    public partial class Form1 : Form
    {
        SqlConnection sqlConnection;
        SqlDataAdapter sqlDataAdapter;
        public Form1()
        {
            InitializeComponent();
            sqlConnection = new SqlConnection("Data Source = DESKTOP-I7BIE28\\SQLSERVER; database = MichalKurpinskiLab3; Trusted_Connection = yes");
        }
        /// <summary>
        /// Przycisk odwołujący się do metody pokazującej wszystkich pracowników.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonGetAllEmployees_Click(object sender, EventArgs e)
        {
            Employee.GetAllEmployees(sqlConnection, sqlDataAdapter, dataGridViewComputerService);
        }
        /// <summary>
        /// Przycisk odwołujący się do metody pokazującej wszystkie naprawy, ktorych koszt jest mniejszy niż wartośc wpisane w textBoxCost.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLowerCost_Click(object sender, EventArgs e)
        {
            Service.GetServices(sqlConnection, sqlDataAdapter, dataGridViewComputerService, textBoxCost);
        }
        /// <summary>
        /// Przycisk odwołujący się do metody pokazującej wszystkie komputery.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonShowAllComputers_Click(object sender, EventArgs e)
        {
            Computer.GetAllComputers(sqlConnection, sqlDataAdapter, dataGridViewComputerService);
        }
        /// <summary>
        /// Przycisk odwołujący się do metody pokazującej wszystkie serwisy, razem z wykonującym naprawę pracownikiem i naprawianym komputerem.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonServices_Click_1(object sender, EventArgs e)
        {
            Service.getAllServices(sqlConnection, sqlDataAdapter, dataGridViewComputerService);
        }
        /// <summary>
        /// Kliknięcie w ktorykolwiek z TextBoxow usuwa z niego wpisany tekst.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_Click(object sender, MouseEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            textBox.Text = "";
        }
        /// <summary>
        /// Przycisk odwołujący się do metody pokazującej wszystkich pracowników, których imię zaczyna się ciągiem wpisanym w textBoxName
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonNameAt_Click(object sender, EventArgs e)
        {
            Employee.getEmployeesNameAt(sqlConnection, sqlDataAdapter, dataGridViewComputerService, textBoxName);
        }
        /// <summary>
        /// Przycisk odwołujący się do metody pokazującej wszystkich pracowników, których wiek znajduje się w przedziale wyznaczonym przez wartości wpisane w odpowiednie TextBoxy.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAgeBetween_Click(object sender, EventArgs e)
        {
            Employee.AgeBetween(sqlConnection, sqlDataAdapter, dataGridViewComputerService, textBoxLowerAge, textBoxHigherAge);
        }
        /// <summary>
        /// Przycisk odwołujący się do metody, która wyświetla wszystkich klientów.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClients_Click(object sender, EventArgs e)
        {
            Client.GetAllClients(sqlConnection, sqlDataAdapter, dataGridViewComputerService);
        }
    }
}
