﻿namespace MichalKurpinskiLab3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewComputerService = new System.Windows.Forms.DataGridView();
            this.labelName = new System.Windows.Forms.Label();
            this.buttonGetAllEmployees = new System.Windows.Forms.Button();
            this.textBoxCost = new System.Windows.Forms.TextBox();
            this.buttonShowAllComputers = new System.Windows.Forms.Button();
            this.buttonServices = new System.Windows.Forms.Button();
            this.buttonLowerCost = new System.Windows.Forms.Button();
            this.buttonNameAt = new System.Windows.Forms.Button();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.buttonAgeBetween = new System.Windows.Forms.Button();
            this.textBoxLowerAge = new System.Windows.Forms.TextBox();
            this.textBoxHigherAge = new System.Windows.Forms.TextBox();
            this.buttonClients = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewComputerService)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewComputerService
            // 
            this.dataGridViewComputerService.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewComputerService.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewComputerService.Location = new System.Drawing.Point(1, 162);
            this.dataGridViewComputerService.Name = "dataGridViewComputerService";
            this.dataGridViewComputerService.RowTemplate.Height = 37;
            this.dataGridViewComputerService.Size = new System.Drawing.Size(1256, 561);
            this.dataGridViewComputerService.TabIndex = 0;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.Location = new System.Drawing.Point(358, 65);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(586, 85);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Michał Kurpiński";
            // 
            // buttonGetAllEmployees
            // 
            this.buttonGetAllEmployees.Location = new System.Drawing.Point(1013, 763);
            this.buttonGetAllEmployees.Name = "buttonGetAllEmployees";
            this.buttonGetAllEmployees.Size = new System.Drawing.Size(241, 56);
            this.buttonGetAllEmployees.TabIndex = 2;
            this.buttonGetAllEmployees.Text = "Employees";
            this.buttonGetAllEmployees.UseVisualStyleBackColor = true;
            this.buttonGetAllEmployees.Click += new System.EventHandler(this.buttonGetAllEmployees_Click);
            // 
            // textBoxCost
            // 
            this.textBoxCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxCost.Location = new System.Drawing.Point(282, 763);
            this.textBoxCost.Multiline = true;
            this.textBoxCost.Name = "textBoxCost";
            this.textBoxCost.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBoxCost.Size = new System.Drawing.Size(131, 56);
            this.textBoxCost.TabIndex = 3;
            this.textBoxCost.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox_Click);
            // 
            // buttonShowAllComputers
            // 
            this.buttonShowAllComputers.Location = new System.Drawing.Point(769, 766);
            this.buttonShowAllComputers.Name = "buttonShowAllComputers";
            this.buttonShowAllComputers.Size = new System.Drawing.Size(220, 56);
            this.buttonShowAllComputers.TabIndex = 5;
            this.buttonShowAllComputers.Text = "Computers";
            this.buttonShowAllComputers.UseVisualStyleBackColor = true;
            this.buttonShowAllComputers.Click += new System.EventHandler(this.buttonShowAllComputers_Click);
            // 
            // buttonServices
            // 
            this.buttonServices.Location = new System.Drawing.Point(519, 766);
            this.buttonServices.Name = "buttonServices";
            this.buttonServices.Size = new System.Drawing.Size(220, 56);
            this.buttonServices.TabIndex = 6;
            this.buttonServices.Text = "Services";
            this.buttonServices.UseVisualStyleBackColor = true;
            this.buttonServices.Click += new System.EventHandler(this.buttonServices_Click_1);
            // 
            // buttonLowerCost
            // 
            this.buttonLowerCost.Location = new System.Drawing.Point(17, 763);
            this.buttonLowerCost.Name = "buttonLowerCost";
            this.buttonLowerCost.Size = new System.Drawing.Size(248, 56);
            this.buttonLowerCost.TabIndex = 4;
            this.buttonLowerCost.Text = "Lower Cost than";
            this.buttonLowerCost.UseVisualStyleBackColor = true;
            this.buttonLowerCost.Click += new System.EventHandler(this.buttonLowerCost_Click);
            // 
            // buttonNameAt
            // 
            this.buttonNameAt.Location = new System.Drawing.Point(17, 852);
            this.buttonNameAt.Name = "buttonNameAt";
            this.buttonNameAt.Size = new System.Drawing.Size(248, 71);
            this.buttonNameAt.TabIndex = 7;
            this.buttonNameAt.Text = "Name Employee At";
            this.buttonNameAt.UseVisualStyleBackColor = true;
            this.buttonNameAt.Click += new System.EventHandler(this.buttonNameAt_Click);
            // 
            // textBoxName
            // 
            this.textBoxName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxName.Location = new System.Drawing.Point(282, 852);
            this.textBoxName.Multiline = true;
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBoxName.Size = new System.Drawing.Size(131, 71);
            this.textBoxName.TabIndex = 8;
            this.textBoxName.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox_Click);
            // 
            // buttonAgeBetween
            // 
            this.buttonAgeBetween.Location = new System.Drawing.Point(584, 849);
            this.buttonAgeBetween.Name = "buttonAgeBetween";
            this.buttonAgeBetween.Size = new System.Drawing.Size(238, 74);
            this.buttonAgeBetween.TabIndex = 9;
            this.buttonAgeBetween.Text = "<Age Employee<";
            this.buttonAgeBetween.UseVisualStyleBackColor = true;
            this.buttonAgeBetween.Click += new System.EventHandler(this.buttonAgeBetween_Click);
            // 
            // textBoxLowerAge
            // 
            this.textBoxLowerAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxLowerAge.Location = new System.Drawing.Point(447, 852);
            this.textBoxLowerAge.Multiline = true;
            this.textBoxLowerAge.Name = "textBoxLowerAge";
            this.textBoxLowerAge.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBoxLowerAge.Size = new System.Drawing.Size(131, 71);
            this.textBoxLowerAge.TabIndex = 10;
            this.textBoxLowerAge.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox_Click);
            // 
            // textBoxHigherAge
            // 
            this.textBoxHigherAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxHigherAge.Location = new System.Drawing.Point(837, 849);
            this.textBoxHigherAge.Multiline = true;
            this.textBoxHigherAge.Name = "textBoxHigherAge";
            this.textBoxHigherAge.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBoxHigherAge.Size = new System.Drawing.Size(131, 74);
            this.textBoxHigherAge.TabIndex = 11;
            this.textBoxHigherAge.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox_Click);
            // 
            // buttonClients
            // 
            this.buttonClients.Location = new System.Drawing.Point(1013, 852);
            this.buttonClients.Name = "buttonClients";
            this.buttonClients.Size = new System.Drawing.Size(241, 71);
            this.buttonClients.TabIndex = 12;
            this.buttonClients.Text = "Clients";
            this.buttonClients.UseVisualStyleBackColor = true;
            this.buttonClients.Click += new System.EventHandler(this.buttonClients_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1280, 1000);
            this.Controls.Add(this.buttonClients);
            this.Controls.Add(this.textBoxHigherAge);
            this.Controls.Add(this.textBoxLowerAge);
            this.Controls.Add(this.buttonAgeBetween);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.buttonNameAt);
            this.Controls.Add(this.buttonServices);
            this.Controls.Add(this.buttonShowAllComputers);
            this.Controls.Add(this.buttonLowerCost);
            this.Controls.Add(this.textBoxCost);
            this.Controls.Add(this.buttonGetAllEmployees);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.dataGridViewComputerService);
            this.Name = "Form1";
            this.Text = "Computer Services";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewComputerService)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewComputerService;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Button buttonGetAllEmployees;
        private System.Windows.Forms.TextBox textBoxCost;
        private System.Windows.Forms.Button buttonShowAllComputers;
        private System.Windows.Forms.Button buttonServices;
        private System.Windows.Forms.Button buttonLowerCost;
        private System.Windows.Forms.Button buttonNameAt;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Button buttonAgeBetween;
        private System.Windows.Forms.TextBox textBoxLowerAge;
        private System.Windows.Forms.TextBox textBoxHigherAge;
        private System.Windows.Forms.Button buttonClients;
    }
}

