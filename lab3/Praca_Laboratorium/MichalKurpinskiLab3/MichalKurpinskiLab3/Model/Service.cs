﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiLab3.Model
{
    class Service
    {
        /// <summary>
        /// Metoda pokazująca wszystkie naprawy, których koszt jest mniejszy niż wartość wpisana do odpowiedniego TextBox'u.
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        /// <param name="textBox"></param>
        public static void GetServices(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView, TextBox textBox)
        {
            try
            {
                dataGridView.DataSource = null;
                sqlDataAdapter = new SqlDataAdapter("Select c.Producent,c.Model, e.Surname as Naprawiający, s.Cost as Koszt, s.Descripction as Opis from Table_Services s join Table_Employees e on e.ID = s.EmployeeID join Table_Computers c on c.ID = s.ComputerID where s.Cost <" + textBox.Text, sqlConnection);
                DataTable dataTable = new DataTable();
                sqlDataAdapter.Fill(dataTable);
                dataGridView.DataSource = dataTable;
            }
            catch(Exception)
            {
                MessageBox.Show("Wpisałes nieodpowienią wartość!", "Error");
            }
        }
        /// <summary>
        /// Metoda wyświetlająca wszystkie naprawy.
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        public static void getAllServices(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView) {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select c.Producent,c.Model, e.Surname as Naprawiający, s.Cost as Koszt, s.Descripction as Opis from Table_Services s join Table_Employees e on e.ID = s.EmployeeID join Table_Computers c on c.ID = s.ComputerID", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
    }
}
