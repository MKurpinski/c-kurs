﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiLab3.Model
{
    public class Employee
    {
        /// <summary>
        /// Metoda pokazująca wszystkich pracowników.
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        public static void GetAllEmployees(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select ID, Name as Imię, Surname as Nazwisko from Table_Employees", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
        /// <summary>
        /// etoda pokazująca wszystkich pracowników, ktorych imię zaczyna się wpisanym do TextBoxa ciągiem.
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        /// <param name="textBox"></param>
        public static void getEmployeesNameAt(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView,TextBox textBox)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select ID, Name as Imię, Surname as Nazwisko from Table_Employees where Name like '" + textBox.Text+"%'", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
        /// <summary>
        /// Metoda pokazująca wszystkich pracowników,których wiek miesci się w przedziale wyznaczonym przez wpisane do odpowiednich TextBoxów wartosci.
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        /// <param name="textBoxLower"></param>
        /// <param name="textBoxHigher"></param>
        public static void AgeBetween(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView, TextBox textBoxLower, TextBox textBoxHigher)
        {
            try
            {
                dataGridView.DataSource = null;
                sqlDataAdapter = new SqlDataAdapter("Select ID, Name as Imię, Surname as Nazwisko, Age as Wiek from Table_Employees where Age>" + textBoxLower.Text + "and Age<" + textBoxHigher.Text, sqlConnection);
                DataTable dataTable = new DataTable();
                sqlDataAdapter.Fill(dataTable);
                dataGridView.DataSource = dataTable;
            }
            catch (Exception) {
                MessageBox.Show("Wpisałeś złe wartości!", "Error!");
            }
        }
    }
}
