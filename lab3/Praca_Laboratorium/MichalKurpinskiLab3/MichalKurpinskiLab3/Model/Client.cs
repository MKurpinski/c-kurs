﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiLab3.Model
{
    class Client
    {
        /// <summary>
        /// Metoda pozwalająca na wyświetlenie wszystkich klientów.
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        public static void GetAllClients(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select c.ID, c.Name as Imię, c.Surname as Nazwisko, com.Model as Komputer from Clients c join Table_Computers com on com.ID = c.ComputerID", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
    }
}
