Insert into Autor(Name,Surname,Country) Values('Micha�','Kurpi�ski','Poland');
declare @autorID int = 0;
Set @autorID = SCOPE_IDENTITY();
Insert into Category(Name) Values('Przygodowa');
declare @categoryID int = 0;
Set @categoryID = SCOPE_IDENTITY();
Insert into Publishing(Name,WWW) Values('Nowe',Null);
declare @publishingID int = 0;
Set @publishingID= SCOPE_IDENTITY();
Insert into Book(Name,AutorID,CategoryID,PublishingHouseID,Price) Values('Nowa Ksi��eczka',@autorID,@categoryID,@publishingID,500);
declare @bookID int =0;
Set @bookID = SCOPE_IDENTITY();
Insert into DeliveryAdress(Adress) Values('nowy Adres')
declare @deliveryAdressID int = 0;
Set @deliveryAdressID = SCOPE_IDENTITY();
Insert into Purchaser(Name,Surname,Email,AdressID) Values('Tomasz','Mikulski','Tomek@email.com',@deliveryAdressID);
declare @purchaserID int =0;
set @purchaserID = SCOPE_IDENTITY();
Insert into Provider(Name) Values ('Dostawca');
declare @providerID int =0;
Set @providerID = SCOPE_IDENTITY();
Insert into Supply(FormOfSupply) Values('forma Dostawy');
declare @supplyID int =0;
Set @supplyID = SCOPE_IDENTITY();
Insert into Payment(Payment) Values('Karta');
declare @paymentID int =0;
Set @paymentID = SCOPE_IDENTITY();
Insert into Orders(BookID,PurchaserID,ProviderID,DeliveryAdressID,SupplyID,PaymentID,Quantity) Values(@bookID,@purchaserID,@providerID,@deliveryAdressID,@supplyID,@paymentID,10);
declare @orderID int =0;
Set @orderID =SCOPE_IDENTITY();
Insert into Status(OrderID,Status) Values(@orderID,'Delivered');
Insert into Store(BookID,Quantity) Values(@bookID,10);


