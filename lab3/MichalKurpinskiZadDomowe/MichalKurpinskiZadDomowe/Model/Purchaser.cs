﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiZadDomowe.Model
{
    class Purchaser
    {
        /// <summary>
        /// Metoda pozwalająca na wyświetlenie wszystkich zamawiających.
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        public static void GetAllPurchasers(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select p.Name as Imię, p.Surname as Nazwisko, p.Email as Email, d.Adress as Adres from Purchaser p join DeliveryAdress d on d.ID = p.AdressID", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
        /// <summary>
        /// Metoda pozwalająca na znalezienie kupującego lub adresu, w zaleznosci od podanego w parametrze metody TextBox'u.
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        /// <param name="textBox"></param>
        public static void FindPurchaser(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView, TextBox textBox)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select p.Name as Imię, p.Surname as Nazwisko, p.Email as Email, d.Adress as Adres from Purchaser p join DeliveryAdress d on d.ID = p.AdressID where d.Adress like '" + textBox.Text + "%'", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

    }
}
