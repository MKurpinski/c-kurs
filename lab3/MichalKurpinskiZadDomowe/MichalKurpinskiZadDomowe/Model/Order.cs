﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiZadDomowe.Model
{
    class Order
    {
        /// <summary>
        /// Metoda wyświetlająca wszystkie zamówienia.
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        public static void GetAllOrders(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select p.Name as Imię, p.Surname as Nazwisko, b.Name as Książka, d.Adress as AdresDostawy, pro.Name as Dostawa, o.Quantity as Ilość, pay.Payment as Płatnosć, b.Price*o.Quantity as Cena from Orders o join Book b on b.ID = o.BookID join Purchaser p on p.ID = o.PurchaserID join DeliveryAdress d on d.ID = o.DeliveryAdressID join Provider pro on pro.ID = o.ProviderID join Payment pay on pay.ID = o.PaymentID", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
        /// <summary>
        /// Metoda wyświetlająca zamówienia, w których cena zamowienia jest nizsza niż wartosć w textBoxCheapier.
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        /// <param name="textBox"></param>
        public static void GetCheapierThan(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView,TextBox textBox)
        {
            try
            {
                dataGridView.DataSource = null;
                sqlDataAdapter = new SqlDataAdapter("Select p.Name as Imię, p.Surname as Nazwisko, b.Name as Książka, d.Adress as AdresDostawy, pro.Name as Dostawa, o.Quantity as Ilość, pay.Payment as Płatnosć, b.Price*o.Quantity as Cena from Orders o join Book b on b.ID = o.BookID join Purchaser p on p.ID = o.PurchaserID join DeliveryAdress d on d.ID = o.DeliveryAdressID join Provider pro on pro.ID = o.ProviderID join Payment pay on pay.ID = o.PaymentID where b.Price*o.Quantity<" + textBox.Text, sqlConnection);
                DataTable dataTable = new DataTable();
                sqlDataAdapter.Fill(dataTable);
                dataGridView.DataSource = dataTable;
            }
            catch (Exception) {
                MessageBox.Show("Dane zostaly wprowadzone nieprawidlowo!", "Error!");
                textBox.Text = "";
            }

        }/// <summary>
        /// metoda wyswietlająca zamówienia opłacane kartą.
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        public static void GetOrdersCard(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select p.Name as Imię, p.Surname as Nazwisko, b.Name as Książka, d.Adress as AdresDostawy, pro.Name as Dostawa, o.Quantity as Ilość, pay.Payment as Płatnosć, b.Price*o.Quantity as Cena from Orders o join Book b on b.ID = o.BookID join Purchaser p on p.ID = o.PurchaserID join DeliveryAdress d on d.ID = o.DeliveryAdressID join Provider pro on pro.ID = o.ProviderID join Payment pay on pay.ID = o.PaymentID where pay.Payment like 'Karta'", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
        /// <summary>
        /// metoda wyswietlająca niedostarczone zamówienia.
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        public static void GetUnFinishedOrders(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select p.Name as Imię, p.Surname as Nazwisko, b.Name as Książka, d.Adress as AdresDostawy, pro.Name as Dostawa, o.Quantity as Ilość, pay.Payment as Płatnosć, b.Price*o.Quantity as Cena, sta.Status from Orders o join Book b on b.ID = o.BookID join Purchaser p on p.ID = o.PurchaserID join DeliveryAdress d on d.ID = o.DeliveryAdressID join Provider pro on pro.ID = o.ProviderID join Payment pay on pay.ID = o.PaymentID join Status sta on o.ID = sta.OrderID where sta.Status not like 'Delivered'", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
        
    }
}
