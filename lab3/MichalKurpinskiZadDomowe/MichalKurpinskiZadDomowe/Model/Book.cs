﻿using MichalKurpinskiZadDomowe.View;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiZadDomowe.Model
{
    class Book
    {
        /// <summary>
        /// Metoda wyświetlająca wszystkie ksiażki
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        public static void GetAllBooks(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select b.Name as Tytuł, a.Name as Imię, a.Surname as Nazwisko, b.Price as Cena, p.Name as Wydawnictwo, s.Quantity as Dostępne from Book b join Autor a on a.ID = b.AutorID join Publishing p on p.ID = b.PublishingHouseID join Store s on s.BookID = b.ID ", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
        /// <summary>
        /// Metoda pozwalająca na wyszukiwanie autora.
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        /// <param name="textBox"></param>
        public static void SearchAutor(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView,TextBox textBox)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select b.Name as Tytuł, a.Name as Imię, a.Surname as Nazwisko, b.Price as Cena, p.Name as Wydawnictwo, s.Quantity as Dostępne from Book b join Autor a on a.ID = b.AutorID join Publishing p on p.ID = b.PublishingHouseID join Store s on s.BookID = b.ID where a.Surname like '"+ textBox.Text+"%'", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
        /// <summary>
        /// Metoda pozwalająca na wyszukiwania ksiązki.
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        /// <param name="textBox"></param>
        public static void SearchBook(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView, TextBox textBox)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select b.Name as Tytuł, a.Name as Imię, a.Surname as Nazwisko, b.Price as Cena, p.Name as Wydawnictwo, s.Quantity as Dostępne from Book b join Autor a on a.ID = b.AutorID join Publishing p on p.ID = b.PublishingHouseID join Store s on s.BookID = b.ID where b.Name like '" + textBox.Text + "%'", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
        /// <summary>
        /// Metoda pozwalająca na wyszukiwanie wydawnictw.
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        /// <param name="textBox"></param>
        public static void SearchPublishingHouse(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView, TextBox textBox)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select b.Name as Tytuł, a.Name as Imię, a.Surname as Nazwisko, b.Price as Cena, p.Name as Wydawnictwo, s.Quantity as Dostępne from Book b join Autor a on a.ID = b.AutorID join Publishing p on p.ID = b.PublishingHouseID join Store s on s.BookID = b.ID where p.Name like '" + textBox.Text + "%'", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
        /// <summary>
        /// Metoda pozwalająca na wyświetlenie książek tańszysz niż wartosć wpisana w textBoxCheaper.
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        /// <param name="textBox"></param>
        public static void BooksCheaperThan(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView, TextBox textBox)
        {
            try
            {
                dataGridView.DataSource = null;
                sqlDataAdapter = new SqlDataAdapter("Select b.Name as Tytuł, a.Name as Imię, a.Surname as Nazwisko, b.Price as Cena, p.Name as Wydawnictwo, s.Quantity as Dostępne from Book b join Autor a on a.ID = b.AutorID join Publishing p on p.ID = b.PublishingHouseID join Store s on s.BookID = b.ID where b.Price<" + textBox.Text + "and s.Quantity>0", sqlConnection);
                DataTable dataTable = new DataTable();
                sqlDataAdapter.Fill(dataTable);
                dataGridView.DataSource = dataTable;
            }
            catch (Exception) {
                MessageBox.Show("Wprowadzono nieprawidłowe dane", "Error!");
                textBox.Text = "";
            }
        }

    }
}
