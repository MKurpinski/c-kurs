﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MichalKurpinskiZadDomowe.Model;

namespace MichalKurpinskiZadDomowe.View
{
    public partial class FormOrder : Form
    {
        FormMain owner;//Własciciel tej formy.
        public FormOrder(FormMain owner)
        {
            InitializeComponent();
            this.owner = owner;
        }
        /// <summary>
        /// Kliknięcie na pzycisk urochamia metodę odpowiedzialną za wyświetlenie wszystkich zamówień.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAllOrders_Click(object sender, EventArgs e)
        {
            Order.GetAllOrders(owner.sqlConnection, owner.sqlDataAdapter, dataGridViewOrder);
        }
        /// <summary>
        /// Kliknięcie na przycisk uruchamia metodę odpowiedzialną za wyświetlenie  zamówień, których koszt za jedną książkę jest mniejszy niż wartość wpisana w textBoxCheapier.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCheapierThan_Click(object sender, EventArgs e)
        {
            Order.GetCheapierThan(owner.sqlConnection, owner.sqlDataAdapter, dataGridViewOrder, textBoxCheapier);
        }
        /// <summary>
        /// Po kliknięciu na którykolwiek textBox jego tekst znika.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_MouseClick(object sender, MouseEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            textBox.Text = "";
        }
        /// <summary>
        /// Kliknięcie na pzycisk uruchamia metodę odpowiedzialną za wyświetlenie  zamówień,które zostały oplacone karta,
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCard_Click(object sender, EventArgs e)
        {
            Order.GetOrdersCard(owner.sqlConnection, owner.sqlDataAdapter, dataGridViewOrder);
        }
        /// <summary>
        /// Kliknięcie na pzycisk uruchamia metodę odpowiedzialną za wyświetlenie  zamówień,które nie zostały dostarczone.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUnfinished_Click(object sender, EventArgs e)
        {
            Order.GetUnFinishedOrders(owner.sqlConnection, owner.sqlDataAdapter, dataGridViewOrder);
        }
    }
}
