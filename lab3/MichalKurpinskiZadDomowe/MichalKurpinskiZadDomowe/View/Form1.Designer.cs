﻿namespace MichalKurpinskiZadDomowe.View
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.labelMenu = new System.Windows.Forms.Label();
            this.buttonOrders = new System.Windows.Forms.Button();
            this.buttonBooks = new System.Windows.Forms.Button();
            this.buttonPurchasers = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelMenu
            // 
            this.labelMenu.AutoSize = true;
            this.labelMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 26F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMenu.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelMenu.Location = new System.Drawing.Point(570, 63);
            this.labelMenu.Name = "labelMenu";
            this.labelMenu.Size = new System.Drawing.Size(271, 89);
            this.labelMenu.TabIndex = 0;
            this.labelMenu.Text = "MENU";
            // 
            // buttonOrders
            // 
            this.buttonOrders.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonOrders.Location = new System.Drawing.Point(91, 390);
            this.buttonOrders.Name = "buttonOrders";
            this.buttonOrders.Size = new System.Drawing.Size(1139, 140);
            this.buttonOrders.TabIndex = 1;
            this.buttonOrders.Text = "Orders";
            this.buttonOrders.UseVisualStyleBackColor = false;
            this.buttonOrders.Click += new System.EventHandler(this.buttonOrders_Click);
            // 
            // buttonBooks
            // 
            this.buttonBooks.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonBooks.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonBooks.Location = new System.Drawing.Point(91, 216);
            this.buttonBooks.Name = "buttonBooks";
            this.buttonBooks.Size = new System.Drawing.Size(535, 141);
            this.buttonBooks.TabIndex = 2;
            this.buttonBooks.Text = "Books";
            this.buttonBooks.UseVisualStyleBackColor = false;
            this.buttonBooks.Click += new System.EventHandler(this.buttonBooks_Click);
            // 
            // buttonPurchasers
            // 
            this.buttonPurchasers.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonPurchasers.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonPurchasers.Location = new System.Drawing.Point(664, 217);
            this.buttonPurchasers.Name = "buttonPurchasers";
            this.buttonPurchasers.Size = new System.Drawing.Size(566, 140);
            this.buttonPurchasers.TabIndex = 3;
            this.buttonPurchasers.Text = "Purchasers";
            this.buttonPurchasers.UseVisualStyleBackColor = false;
            this.buttonPurchasers.Click += new System.EventHandler(this.buttonPurchasers_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1297, 698);
            this.Controls.Add(this.buttonPurchasers);
            this.Controls.Add(this.buttonBooks);
            this.Controls.Add(this.buttonOrders);
            this.Controls.Add(this.labelMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "Księgarnia Kredek";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelMenu;
        private System.Windows.Forms.Button buttonOrders;
        private System.Windows.Forms.Button buttonBooks;
        private System.Windows.Forms.Button buttonPurchasers;
    }
}

