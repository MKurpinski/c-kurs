﻿namespace MichalKurpinskiZadDomowe.View
{
    partial class FormBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBook));
            this.dataGridViewBook = new System.Windows.Forms.DataGridView();
            this.buttonAllBooks = new System.Windows.Forms.Button();
            this.buttonSearchAutor = new System.Windows.Forms.Button();
            this.buttonSearchBook = new System.Windows.Forms.Button();
            this.textBoxAutor = new System.Windows.Forms.TextBox();
            this.textBoxBook = new System.Windows.Forms.TextBox();
            this.buttonSearchPublishing = new System.Windows.Forms.Button();
            this.buttonCheaperThan = new System.Windows.Forms.Button();
            this.textBoxSearchPublishing = new System.Windows.Forms.TextBox();
            this.textBoxCheaper = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBook)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewBook
            // 
            this.dataGridViewBook.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewBook.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridViewBook.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBook.Location = new System.Drawing.Point(31, 77);
            this.dataGridViewBook.Name = "dataGridViewBook";
            this.dataGridViewBook.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataGridViewBook.RowTemplate.Height = 37;
            this.dataGridViewBook.Size = new System.Drawing.Size(1422, 520);
            this.dataGridViewBook.TabIndex = 0;
            // 
            // buttonAllBooks
            // 
            this.buttonAllBooks.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonAllBooks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAllBooks.Location = new System.Drawing.Point(31, 647);
            this.buttonAllBooks.Name = "buttonAllBooks";
            this.buttonAllBooks.Size = new System.Drawing.Size(1422, 76);
            this.buttonAllBooks.TabIndex = 1;
            this.buttonAllBooks.Text = "All Books";
            this.buttonAllBooks.UseVisualStyleBackColor = false;
            this.buttonAllBooks.Click += new System.EventHandler(this.buttonAllBooks_Click);
            // 
            // buttonSearchAutor
            // 
            this.buttonSearchAutor.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonSearchAutor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSearchAutor.Location = new System.Drawing.Point(106, 761);
            this.buttonSearchAutor.Name = "buttonSearchAutor";
            this.buttonSearchAutor.Size = new System.Drawing.Size(240, 93);
            this.buttonSearchAutor.TabIndex = 2;
            this.buttonSearchAutor.Text = "Find Autor";
            this.buttonSearchAutor.UseVisualStyleBackColor = false;
            this.buttonSearchAutor.Click += new System.EventHandler(this.buttonSearchAutor_Click);
            // 
            // buttonSearchBook
            // 
            this.buttonSearchBook.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonSearchBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSearchBook.Location = new System.Drawing.Point(106, 881);
            this.buttonSearchBook.Name = "buttonSearchBook";
            this.buttonSearchBook.Size = new System.Drawing.Size(240, 92);
            this.buttonSearchBook.TabIndex = 3;
            this.buttonSearchBook.Text = "Find Book";
            this.buttonSearchBook.UseVisualStyleBackColor = false;
            this.buttonSearchBook.Click += new System.EventHandler(this.buttonSearchBook_Click);
            // 
            // textBoxAutor
            // 
            this.textBoxAutor.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxAutor.Location = new System.Drawing.Point(430, 761);
            this.textBoxAutor.Multiline = true;
            this.textBoxAutor.Name = "textBoxAutor";
            this.textBoxAutor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxAutor.Size = new System.Drawing.Size(224, 93);
            this.textBoxAutor.TabIndex = 4;
            // 
            // textBoxBook
            // 
            this.textBoxBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxBook.Location = new System.Drawing.Point(430, 891);
            this.textBoxBook.Multiline = true;
            this.textBoxBook.Name = "textBoxBook";
            this.textBoxBook.Size = new System.Drawing.Size(224, 79);
            this.textBoxBook.TabIndex = 5;
            // 
            // buttonSearchPublishing
            // 
            this.buttonSearchPublishing.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonSearchPublishing.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSearchPublishing.Location = new System.Drawing.Point(769, 761);
            this.buttonSearchPublishing.Name = "buttonSearchPublishing";
            this.buttonSearchPublishing.Size = new System.Drawing.Size(252, 93);
            this.buttonSearchPublishing.TabIndex = 6;
            this.buttonSearchPublishing.Text = "Find PublishingHouse";
            this.buttonSearchPublishing.UseVisualStyleBackColor = false;
            this.buttonSearchPublishing.Click += new System.EventHandler(this.buttonSearchPublishing_Click);
            // 
            // buttonCheaperThan
            // 
            this.buttonCheaperThan.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonCheaperThan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonCheaperThan.Location = new System.Drawing.Point(769, 881);
            this.buttonCheaperThan.Name = "buttonCheaperThan";
            this.buttonCheaperThan.Size = new System.Drawing.Size(252, 92);
            this.buttonCheaperThan.TabIndex = 7;
            this.buttonCheaperThan.Text = "Cheaper than";
            this.buttonCheaperThan.UseVisualStyleBackColor = false;
            this.buttonCheaperThan.Click += new System.EventHandler(this.buttonCheaperThan_Click);
            // 
            // textBoxSearchPublishing
            // 
            this.textBoxSearchPublishing.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxSearchPublishing.Location = new System.Drawing.Point(1102, 761);
            this.textBoxSearchPublishing.Multiline = true;
            this.textBoxSearchPublishing.Name = "textBoxSearchPublishing";
            this.textBoxSearchPublishing.Size = new System.Drawing.Size(224, 93);
            this.textBoxSearchPublishing.TabIndex = 8;
            // 
            // textBoxCheaper
            // 
            this.textBoxCheaper.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxCheaper.Location = new System.Drawing.Point(1102, 891);
            this.textBoxCheaper.Multiline = true;
            this.textBoxCheaper.Name = "textBoxCheaper";
            this.textBoxCheaper.Size = new System.Drawing.Size(224, 79);
            this.textBoxCheaper.TabIndex = 9;
            // 
            // FormBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1479, 996);
            this.Controls.Add(this.textBoxCheaper);
            this.Controls.Add(this.textBoxSearchPublishing);
            this.Controls.Add(this.buttonCheaperThan);
            this.Controls.Add(this.buttonSearchPublishing);
            this.Controls.Add(this.textBoxBook);
            this.Controls.Add(this.textBoxAutor);
            this.Controls.Add(this.buttonSearchBook);
            this.Controls.Add(this.buttonSearchAutor);
            this.Controls.Add(this.buttonAllBooks);
            this.Controls.Add(this.dataGridViewBook);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormBook";
            this.Text = "Ksiązki";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBook)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewBook;
        private System.Windows.Forms.Button buttonAllBooks;
        private System.Windows.Forms.Button buttonSearchAutor;
        private System.Windows.Forms.Button buttonSearchBook;
        private System.Windows.Forms.TextBox textBoxAutor;
        private System.Windows.Forms.TextBox textBoxBook;
        private System.Windows.Forms.Button buttonSearchPublishing;
        private System.Windows.Forms.Button buttonCheaperThan;
        private System.Windows.Forms.TextBox textBoxSearchPublishing;
        private System.Windows.Forms.TextBox textBoxCheaper;
    }
}