﻿namespace MichalKurpinskiZadDomowe.View
{
    partial class PurchaserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PurchaserForm));
            this.dataGridViewPurchaser = new System.Windows.Forms.DataGridView();
            this.buttonAllPurchasers = new System.Windows.Forms.Button();
            this.buttonFindPurchaser = new System.Windows.Forms.Button();
            this.textBoxPurchaser = new System.Windows.Forms.TextBox();
            this.buttonFindAdress = new System.Windows.Forms.Button();
            this.textBoxAdress = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPurchaser)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewPurchaser
            // 
            this.dataGridViewPurchaser.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewPurchaser.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridViewPurchaser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPurchaser.Location = new System.Drawing.Point(28, 71);
            this.dataGridViewPurchaser.Name = "dataGridViewPurchaser";
            this.dataGridViewPurchaser.RowTemplate.Height = 37;
            this.dataGridViewPurchaser.Size = new System.Drawing.Size(1316, 572);
            this.dataGridViewPurchaser.TabIndex = 0;
            // 
            // buttonAllPurchasers
            // 
            this.buttonAllPurchasers.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonAllPurchasers.Location = new System.Drawing.Point(28, 685);
            this.buttonAllPurchasers.Name = "buttonAllPurchasers";
            this.buttonAllPurchasers.Size = new System.Drawing.Size(1316, 83);
            this.buttonAllPurchasers.TabIndex = 1;
            this.buttonAllPurchasers.Text = "All purchasers";
            this.buttonAllPurchasers.UseVisualStyleBackColor = false;
            this.buttonAllPurchasers.Click += new System.EventHandler(this.buttonAllPurchasers_Click);
            // 
            // buttonFindPurchaser
            // 
            this.buttonFindPurchaser.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonFindPurchaser.Location = new System.Drawing.Point(28, 790);
            this.buttonFindPurchaser.Name = "buttonFindPurchaser";
            this.buttonFindPurchaser.Size = new System.Drawing.Size(726, 71);
            this.buttonFindPurchaser.TabIndex = 2;
            this.buttonFindPurchaser.Text = "Find Purchaser";
            this.buttonFindPurchaser.UseVisualStyleBackColor = false;
            this.buttonFindPurchaser.Click += new System.EventHandler(this.buttonFindPurchaser_Click);
            // 
            // textBoxPurchaser
            // 
            this.textBoxPurchaser.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxPurchaser.Location = new System.Drawing.Point(824, 790);
            this.textBoxPurchaser.Multiline = true;
            this.textBoxPurchaser.Name = "textBoxPurchaser";
            this.textBoxPurchaser.Size = new System.Drawing.Size(520, 71);
            this.textBoxPurchaser.TabIndex = 3;
            // 
            // buttonFindAdress
            // 
            this.buttonFindAdress.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonFindAdress.Location = new System.Drawing.Point(28, 885);
            this.buttonFindAdress.Name = "buttonFindAdress";
            this.buttonFindAdress.Size = new System.Drawing.Size(726, 71);
            this.buttonFindAdress.TabIndex = 4;
            this.buttonFindAdress.Text = "Find Adress";
            this.buttonFindAdress.UseVisualStyleBackColor = false;
            this.buttonFindAdress.Click += new System.EventHandler(this.buttonFindAdress_Click);
            // 
            // textBoxAdress
            // 
            this.textBoxAdress.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxAdress.Location = new System.Drawing.Point(824, 885);
            this.textBoxAdress.Multiline = true;
            this.textBoxAdress.Name = "textBoxAdress";
            this.textBoxAdress.Size = new System.Drawing.Size(520, 71);
            this.textBoxAdress.TabIndex = 5;
            // 
            // PurchaserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1387, 988);
            this.Controls.Add(this.textBoxAdress);
            this.Controls.Add(this.buttonFindAdress);
            this.Controls.Add(this.textBoxPurchaser);
            this.Controls.Add(this.buttonFindPurchaser);
            this.Controls.Add(this.buttonAllPurchasers);
            this.Controls.Add(this.dataGridViewPurchaser);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PurchaserForm";
            this.Text = "Zamawiający";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPurchaser)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewPurchaser;
        private System.Windows.Forms.Button buttonAllPurchasers;
        private System.Windows.Forms.Button buttonFindPurchaser;
        private System.Windows.Forms.TextBox textBoxPurchaser;
        private System.Windows.Forms.Button buttonFindAdress;
        private System.Windows.Forms.TextBox textBoxAdress;
    }
}