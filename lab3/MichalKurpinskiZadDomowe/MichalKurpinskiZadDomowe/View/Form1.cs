﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MichalKurpinskiZadDomowe.Model;
using System.Media;

namespace MichalKurpinskiZadDomowe.View
{
    
    public partial class FormMain : Form
    {
         public SqlConnection sqlConnection;
         public SqlDataAdapter sqlDataAdapter;
        public FormMain()
        {
            InitializeComponent();
            sqlConnection = new SqlConnection("Data Source = DESKTOP-I7BIE28\\SQLSERVER; database = MichalKurpinskiZadDomowe; Trusted_Connection = yes");
        }
        /// <summary>
        /// Wejscie z Menu do okna zamówień.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOrders_Click(object sender, EventArgs e)
        {
            FormOrder formOrder = new FormOrder(this);
            formOrder.ShowDialog();
        }
        /// <summary>
        /// Wejscie z Menu do okna zamówień.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBooks_Click(object sender, EventArgs e)
        {
            FormBook formBook =new FormBook(this);
            formBook.ShowDialog();
        }
        /// <summary>
        /// Wejscie z Menu do okna Zamawiających.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPurchasers_Click(object sender, EventArgs e)
        {
            PurchaserForm purchaserForm = new PurchaserForm(this);
            purchaserForm.ShowDialog();
        }

    }
}
