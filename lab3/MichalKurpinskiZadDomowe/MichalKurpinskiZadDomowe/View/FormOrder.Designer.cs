﻿namespace MichalKurpinskiZadDomowe.View
{
    partial class FormOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOrder));
            this.dataGridViewOrder = new System.Windows.Forms.DataGridView();
            this.buttonAllOrders = new System.Windows.Forms.Button();
            this.buttonCheaperThan = new System.Windows.Forms.Button();
            this.textBoxCheapier = new System.Windows.Forms.TextBox();
            this.buttonCard = new System.Windows.Forms.Button();
            this.buttonUnfinished = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrder)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewOrder
            // 
            this.dataGridViewOrder.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewOrder.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridViewOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewOrder.Location = new System.Drawing.Point(38, 30);
            this.dataGridViewOrder.Name = "dataGridViewOrder";
            this.dataGridViewOrder.RowTemplate.Height = 37;
            this.dataGridViewOrder.Size = new System.Drawing.Size(1581, 477);
            this.dataGridViewOrder.TabIndex = 0;
            // 
            // buttonAllOrders
            // 
            this.buttonAllOrders.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonAllOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAllOrders.Location = new System.Drawing.Point(38, 585);
            this.buttonAllOrders.Name = "buttonAllOrders";
            this.buttonAllOrders.Size = new System.Drawing.Size(437, 115);
            this.buttonAllOrders.TabIndex = 1;
            this.buttonAllOrders.Text = "All Orders";
            this.buttonAllOrders.UseVisualStyleBackColor = false;
            this.buttonAllOrders.Click += new System.EventHandler(this.buttonAllOrders_Click);
            // 
            // buttonCheaperThan
            // 
            this.buttonCheaperThan.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonCheaperThan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonCheaperThan.Location = new System.Drawing.Point(546, 770);
            this.buttonCheaperThan.Name = "buttonCheaperThan";
            this.buttonCheaperThan.Size = new System.Drawing.Size(388, 75);
            this.buttonCheaperThan.TabIndex = 2;
            this.buttonCheaperThan.Text = "Cheapier than";
            this.buttonCheaperThan.UseVisualStyleBackColor = false;
            this.buttonCheaperThan.Click += new System.EventHandler(this.buttonCheapierThan_Click);
            // 
            // textBoxCheapier
            // 
            this.textBoxCheapier.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxCheapier.Location = new System.Drawing.Point(1006, 770);
            this.textBoxCheapier.Multiline = true;
            this.textBoxCheapier.Name = "textBoxCheapier";
            this.textBoxCheapier.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBoxCheapier.Size = new System.Drawing.Size(136, 75);
            this.textBoxCheapier.TabIndex = 3;
            this.textBoxCheapier.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox_MouseClick);
            // 
            // buttonCard
            // 
            this.buttonCard.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonCard.Location = new System.Drawing.Point(546, 585);
            this.buttonCard.Name = "buttonCard";
            this.buttonCard.Size = new System.Drawing.Size(517, 115);
            this.buttonCard.TabIndex = 4;
            this.buttonCard.Text = "Orders paid with card";
            this.buttonCard.UseVisualStyleBackColor = false;
            this.buttonCard.Click += new System.EventHandler(this.buttonCard_Click);
            // 
            // buttonUnfinished
            // 
            this.buttonUnfinished.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonUnfinished.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonUnfinished.Location = new System.Drawing.Point(1125, 585);
            this.buttonUnfinished.Name = "buttonUnfinished";
            this.buttonUnfinished.Size = new System.Drawing.Size(494, 115);
            this.buttonUnfinished.TabIndex = 5;
            this.buttonUnfinished.Text = "Undelivered Orders";
            this.buttonUnfinished.UseVisualStyleBackColor = false;
            this.buttonUnfinished.Click += new System.EventHandler(this.buttonUnfinished_Click);
            // 
            // FormOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1658, 888);
            this.Controls.Add(this.buttonUnfinished);
            this.Controls.Add(this.buttonCard);
            this.Controls.Add(this.textBoxCheapier);
            this.Controls.Add(this.buttonCheaperThan);
            this.Controls.Add(this.buttonAllOrders);
            this.Controls.Add(this.dataGridViewOrder);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormOrder";
            this.Text = "Zamówienia";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrder)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewOrder;
        private System.Windows.Forms.Button buttonAllOrders;
        private System.Windows.Forms.Button buttonCheaperThan;
        private System.Windows.Forms.TextBox textBoxCheapier;
        private System.Windows.Forms.Button buttonCard;
        private System.Windows.Forms.Button buttonUnfinished;
    }
}