﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MichalKurpinskiZadDomowe.Model;

namespace MichalKurpinskiZadDomowe.View
{
    public partial class PurchaserForm : Form
    {
        FormMain owner;
        public PurchaserForm(FormMain owner)
        {
            InitializeComponent();
            this.owner = owner;
        }
        /// <summary>
        /// Wyswietlenie wszystkich kupujących
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAllPurchasers_Click(object sender, EventArgs e)
        {
            Purchaser.GetAllPurchasers(owner.sqlConnection, owner.sqlDataAdapter, dataGridViewPurchaser);
        }
        /// <summary>
        /// Znalezienie Kupującego po nazwisku
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFindPurchaser_Click(object sender, EventArgs e)
        {
            Purchaser.FindPurchaser(owner.sqlConnection, owner.sqlDataAdapter, dataGridViewPurchaser, textBoxPurchaser);
        }
        /// <summary>
        /// Znalezienie adresu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFindAdress_Click(object sender, EventArgs e)
        {
            Purchaser.FindPurchaser(owner.sqlConnection, owner.sqlDataAdapter, dataGridViewPurchaser, textBoxAdress);
        }
    }
}
