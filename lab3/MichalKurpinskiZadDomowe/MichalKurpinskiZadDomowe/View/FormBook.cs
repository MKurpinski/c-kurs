﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MichalKurpinskiZadDomowe.Model;

namespace MichalKurpinskiZadDomowe.View
{
    public partial class FormBook : Form
    {
        FormMain owner;//Własciciel tej formy.
        public FormBook(FormMain owner)
        {
            InitializeComponent();
            this.owner = owner;
        }
        /// <summary>
        /// Metoda pozwalajaca na wyswietlenie wszystkich ksiązek.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAllBooks_Click(object sender, EventArgs e)
        {
            Book.GetAllBooks(owner.sqlConnection, owner.sqlDataAdapter, dataGridViewBook);
        }
        /// <summary>
        /// Metoda pozwalajaca na wyszukanie autora i wyswietlenie jego ksiązek.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSearchAutor_Click(object sender, EventArgs e)
        {
            Book.SearchAutor(owner.sqlConnection, owner.sqlDataAdapter, dataGridViewBook, textBoxAutor);
        }
        /// <summary>
        /// Metoda pozwalająca na wyszukanie ksiązki i wyswietlenie jej.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSearchBook_Click(object sender, EventArgs e)
        {
            Book.SearchBook(owner.sqlConnection, owner.sqlDataAdapter, dataGridViewBook, textBoxBook);
        }
        /// <summary>
        /// Metoda pozwalająca na wyszukanie wydawnictwa i wyswietlenie jego ksiązek.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSearchPublishing_Click(object sender, EventArgs e)
        {
            Book.SearchPublishingHouse(owner.sqlConnection, owner.sqlDataAdapter, dataGridViewBook, textBoxSearchPublishing);
        }
        /// <summary>
        /// Metoda pozwalająca na wyświetlenie ksiązek tańszysz niż wartośc wpisana do textBoxCheaper i dostepnych(ilość>0).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCheaperThan_Click(object sender, EventArgs e)
        {
            Book.BooksCheaperThan(owner.sqlConnection, owner.sqlDataAdapter, dataGridViewBook, textBoxCheaper);
        }
    }
}
