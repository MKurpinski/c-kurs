﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc.Html;

namespace DawidKobierskiLab5.Models
{
    public  class Order
    {
        public int Id { get; set; }
        [Display(Name="Imię")]
        public string UserName { get; set; }
        [Display(Name = "Adres")]
        public string UserAdress { get; set; }
        public string Pizzas { get; set; }

        public Order(string pizzas)
        {
            Pizzas = pizzas;
        }

        public Order() { }
    }
}