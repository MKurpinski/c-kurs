﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;

namespace DawidKobierskiLab5.Models
{
    public class Pizza
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Ingredients { get; set; }
        public bool IsOrdered { get; set; }

        public Pizza()
        {
            IsOrdered = false;
        }

    }
}