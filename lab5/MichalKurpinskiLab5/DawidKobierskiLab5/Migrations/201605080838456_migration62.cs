namespace DawidKobierskiLab5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migration62 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Pizzas", "Quantity");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Pizzas", "Quantity", c => c.Int(nullable: false));
        }
    }
}
