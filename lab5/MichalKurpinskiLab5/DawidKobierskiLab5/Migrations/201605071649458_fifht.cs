namespace DawidKobierskiLab5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fifht : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.OrderedPizzas", newName: "Pizzas");
            DropColumn("dbo.Pizzas", "Discriminator");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Pizzas", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            RenameTable(name: "dbo.Pizzas", newName: "OrderedPizzas");
        }
    }
}
