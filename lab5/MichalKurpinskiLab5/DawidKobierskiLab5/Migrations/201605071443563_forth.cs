namespace DawidKobierskiLab5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class forth : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Pizzas", newName: "OrderedPizzas");
            AddColumn("dbo.OrderedPizzas", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            DropTable("dbo.Orders");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PizzaId = c.Int(nullable: false),
                        Name = c.String(),
                        Adress = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.OrderedPizzas", "Discriminator");
            RenameTable(name: "dbo.OrderedPizzas", newName: "Pizzas");
        }
    }
}
