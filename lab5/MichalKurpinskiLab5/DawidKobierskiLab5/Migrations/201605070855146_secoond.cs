namespace DawidKobierskiLab5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class secoond : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pizzas", "isOrdered", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pizzas", "isOrdered");
        }
    }
}
