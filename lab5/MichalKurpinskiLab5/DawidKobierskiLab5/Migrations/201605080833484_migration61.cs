namespace DawidKobierskiLab5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migration61 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pizzas", "Quantity", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pizzas", "Quantity");
        }
    }
}
