namespace DawidKobierskiLab5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migration6 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        UserAdress = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Pizzas", "Order_Id", c => c.Int());
            CreateIndex("dbo.Pizzas", "Order_Id");
            AddForeignKey("dbo.Pizzas", "Order_Id", "dbo.Orders", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pizzas", "Order_Id", "dbo.Orders");
            DropIndex("dbo.Pizzas", new[] { "Order_Id" });
            DropColumn("dbo.Pizzas", "Order_Id");
            DropTable("dbo.Orders");
        }
    }
}
