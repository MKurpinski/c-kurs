namespace DawidKobierskiLab5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migration63 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Pizzas", "Order_Id", "dbo.Orders");
            DropIndex("dbo.Pizzas", new[] { "Order_Id" });
            AddColumn("dbo.Orders", "Pizzas", c => c.String());
            DropColumn("dbo.Pizzas", "Order_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Pizzas", "Order_Id", c => c.Int());
            DropColumn("dbo.Orders", "Pizzas");
            CreateIndex("dbo.Pizzas", "Order_Id");
            AddForeignKey("dbo.Pizzas", "Order_Id", "dbo.Orders", "Id");
        }
    }
}
