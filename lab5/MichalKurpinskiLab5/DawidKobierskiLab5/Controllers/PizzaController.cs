﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DawidKobierskiLab5.Models;

namespace DawidKobierskiLab5.Controllers
{
    public class PizzaController : Controller
    {
        // GET: Pizza
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        /// <summary>
        /// Zwraca listę pizz do widoku Menu.
        /// </summary>
        /// <returns></returns>
        public ActionResult List()
        {
            List<Pizza> pizzas;

            using (var ctx = new EFDbContext())
            {
                pizzas = ctx.Pizzas.ToList();
            }
            return View(pizzas);
        }
        /// <summary>
        /// Dodaje pizzę do Menu.
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            return View(new Pizza());
        }

        [HttpPost]
        public ActionResult Add(Pizza pizza)
        {
            if (!ModelState.IsValid)
            {
                return View(new Pizza());
            }
            using (var ctx = new EFDbContext())
            {
                ctx.Pizzas.Add(pizza);
                ctx.SaveChanges();
            }

            return RedirectToAction("List");
        }

        /// <summary>
        /// Edytuje wybraną pizzę z menu.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            Pizza pizza;
            using (var ctx = new EFDbContext())
            {
                pizza = ctx.Pizzas.FirstOrDefault(p => p.Id == id);
            }
            return View(pizza);
        }

        [HttpPost]
        public ActionResult Edit(Pizza model, int id)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Edit");
            }
            Pizza pizza;
            using (var ctx = new EFDbContext())
            {
                pizza = ctx.Pizzas.FirstOrDefault(p => p.Id == id);
                pizza.Name = model.Name;
                pizza.Ingredients = model.Ingredients;
                ctx.SaveChanges();
            }
            return RedirectToAction("List");
        }
        /// <summary>
        /// usuwa wybraną pizze z menu.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            Pizza pizza;
            using (var ctx = new EFDbContext())
            {
                pizza = ctx.Pizzas.Find(id);
                ctx.Pizzas.Remove(pizza);
                ctx.SaveChanges();
            }
            return RedirectToAction("List");
        }
        /// <summary>
        /// Dodaje wybraną pizzę do koszyka.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Order(int id)
        {
            Pizza pizza;
            List<Pizza> pizzas;
            using (var ctx = new EFDbContext())
            {
                ctx.Pizzas.Find(id).IsOrdered = true;
                pizza = ctx.Pizzas.Find(id);
                pizzas = ctx.Pizzas.Where(x => x.IsOrdered == true).ToList();
                pizzas.Add(pizza);
                ctx.SaveChanges();
            }
            return RedirectToAction("List");
        }
        /// <summary>
        /// Zwraca listę pizz do widoku koszyka.
        /// </summary>
        /// <returns></returns>
        public ActionResult Orders()
        {
            List<Pizza> pizzas;
            using (var ctx = new EFDbContext())
            {
                pizzas = ctx.Pizzas.Where(x => x.IsOrdered == true).ToList();
            }
            return View(pizzas);
        }
        /// <summary>
        /// usuwa wybrana pizze z koszyka.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Cancel(int id)
        {

            using (var ctx = new EFDbContext())
            {
                ctx.Pizzas.Find(id).IsOrdered = false;
                ctx.SaveChanges();
            }
            return RedirectToAction("Orders");
        }
    }
}