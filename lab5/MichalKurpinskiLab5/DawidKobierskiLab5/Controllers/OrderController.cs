﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DawidKobierskiLab5.Models;

namespace DawidKobierskiLab5.Controllers
{
    public class OrderController : Controller
    {
        // GET: Order
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }
        /// <summary>
        /// Potwierdza zamówienie pizz bedacych w koszyku
        /// </summary>
        /// <returns></returns>
        public ActionResult Confirm()
        {
            return View(new Order());
        }

        [HttpPost]
        public ActionResult Confirm(Order order)
        {

            using (var ctx = new EFDbContext())
            {
                if (!ModelState.IsValid)
                {
                    return View(new Order());
                }
                var pizzas = "";
                foreach (var pizza in ctx.Pizzas.Where(x=>x.IsOrdered==true).ToList())
                {
                    pizzas += pizza.Name + ",";
                }
                order.Pizzas = pizzas;
                ctx.Orders.Add(order);
                foreach (var pizza in ctx.Pizzas.Where(x => x.IsOrdered == true).ToList())
                {
                    pizza.IsOrdered = false;
                }
                ctx.SaveChanges();
            }
            return RedirectToAction("OK");
        }
        /// <summary>
        /// wyswietla widok potwierdzajacy dokonanie zamówienia.
        /// </summary>
        /// <returns></returns>
        public ActionResult OK()
        {
            return View();
        }
        /// <summary>
        /// Zwraca listę wszystkich zamówien do widoku Zamówien(wyswietlenie zamowien)
        /// </summary>
        /// <returns></returns>
        public ActionResult Purchased()
        {
            List<Order> orders;
            using (var ctx = new EFDbContext())
            {
                orders = ctx.Orders.ToList();
            }
            return View(orders);
        }
        /// <summary>
        /// usuwa wybrane zamówienie z listy.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delivered(int id)
        {
            Order order;
            using (var ctx = new EFDbContext())
            {
                order = ctx.Orders.Find(id);
                ctx.Orders.Remove(order);
                ctx.SaveChanges();
            }
            return RedirectToAction("Purchased");
        }
    }
}