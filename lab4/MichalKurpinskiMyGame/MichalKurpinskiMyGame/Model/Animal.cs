﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MichalKurpinskiMyGame.Model
{
    public class Animal:Entity
    {
        public Animal()
        {
        }
        public string Name { get; set; }
        public int Age { get; set; }
        public int Health { get; set; }
        public int Cleanliness { get; set; }
        public int Happiness { get; set; }
        public int Hungry { get; set; }
    }
}
