﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MichalKurpinskiMyGame.Model
{
    /// <summary>
    /// Klasa Użytkownika(własciciela)
    /// </summary>
    public class Owner:Entity
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public int AnimalId { get; set; }
        public int Wallet { get; set; }
        public int Score { get; set; }
        
    }
}
