namespace MichalKurpinskiMyGame.Model
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class GameContext : DbContext
    {
        // Your context has been configured to use a 'GameContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'MichalKurpinskiMyGame.Model.GameContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'GameContext' 
        // connection string in the application configuration file.
        public GameContext()
            : base("name=GameContext")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<GameContext>());
        }
            public virtual DbSet<Owner> Owners { get; set; }
            public virtual DbSet<Animal> Animals { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}