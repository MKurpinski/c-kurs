﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MichalKurpinskiMyGame.Repository.Query.Interfaces
{
    public interface IReadRepository<T> where T : class
    {
        T GetById(int id);
        List<T> GetAll();
    }
}
