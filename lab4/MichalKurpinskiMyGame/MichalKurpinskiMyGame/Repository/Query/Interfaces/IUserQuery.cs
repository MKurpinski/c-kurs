﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MichalKurpinskiMyGame.Model;

namespace MichalKurpinskiMyGame.Repository.Query.Interfaces
{
    public interface IUserQuery
    {
        Owner GetOwnerByLoginAndPassword(string name, string password);
        Owner GetByLogin(string login);
        int GetAnimalId(int id);
        List<Owner> GetAll();
    }
    
}
