﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;
using MichalKurpinskiMyGame.Repository.Query.Interfaces;
using MichalKurpinskiMyGame.Model;

namespace MichalKurpinskiMyGame.Repository.Query
{
    public class UserQuery:IUserQuery
    {
        protected readonly GameContext _context;

        public UserQuery(GameContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Zwraca użytkownika po loginie i hasle.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Owner GetOwnerByLoginAndPassword(string name,string password)
        {
            return _context.Set<Owner>().Where(x => x.Name == name && x.Password == password).FirstOrDefault();
        }
        /// <summary>
        /// Zwraca użytkownika po loginie.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public Owner GetByLogin(string login)
        {
            return _context.Set<Owner>().Where(x => x.Name == login).FirstOrDefault();
        }
        /// <summary>
        /// Zwraca id zwięrzęcia.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int GetAnimalId(int id)
        {
            Owner owner = _context.Set<Owner>().Where(x => x.AnimalId == id).FirstOrDefault();
            return owner.AnimalId;
        }
        /// <summary>
        /// Zwraca listę wszystkich użytkowników
        /// </summary>
        /// <returns></returns>
        public List<Owner> GetAll()
        {
            return _context.Set<Owner>().OrderByDescending(x=> x.Score).ToList();
        }
    }
}
