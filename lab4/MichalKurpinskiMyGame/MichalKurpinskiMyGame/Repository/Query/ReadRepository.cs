﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MichalKurpinskiMyGame.Model;
using MichalKurpinskiMyGame.Repository.Query.Interfaces;

namespace MichalKurpinskiMyGame.Repository.Query
{
    public class ReadRepository<T> : IReadRepository<T> where T : Entity
    {
        protected readonly GameContext _context;

        public ReadRepository(GameContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Wyszukuje i zwraca encję po ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T GetById(int id)
        {
            return _context.Set<T>().Where(t => t.Id == id).FirstOrDefault();
        }

        public List<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }
    }
}

