﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MichalKurpinskiMyGame.Repository.Command.Interfaces
{
    public interface IWriteRepository<in T> where T:class
    {
        void Delete(T entity);
        void Save(T entity);
        void Update(T entity);
    }
}
