﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MichalKurpinskiMyGame.Model;
using MichalKurpinskiMyGame.Repository.Command.Interfaces;

namespace MichalKurpinskiMyGame.Repository.Command
{
    public class WriteRepository<T>:IWriteRepository<T> where T:Entity
    {
        private readonly GameContext _context;
        public WriteRepository(GameContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Usuwa encję z bazy.
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }
        /// <summary>
        /// Dodaje encję do bazy.
        /// </summary>
        /// <param name="entity"></param>
        public void Save(T entity)
        {
            _context.Set<T>().Add(entity);
        }
        /// <summary>
        /// uaktualnia encję w bazie.
        /// </summary>
        /// <param name="entity"></param>
        public void Update(T entity)
        {
            _context.Entry(entity);
        }
    }
}
