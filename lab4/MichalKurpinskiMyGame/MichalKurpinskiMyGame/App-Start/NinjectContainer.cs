﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MichalKurpinskiMyGame.Model;
using MichalKurpinskiMyGame.Repository.Command.Interfaces;
using MichalKurpinskiMyGame.Repository.Query;
using MichalKurpinskiMyGame.Repository.Query.Interfaces;
using Ninject;
using Ninject.Modules;
using MichalKurpinskiMyGame.Repository.Command;

namespace MichalKurpinskiMyGame.App_Start
{
    class NinjectContainer : NinjectModule
    {
        private readonly IKernel _kernel;

        public NinjectContainer(IKernel kernel)
        {
            _kernel = kernel;
        }
        public override void Load()
        {
            _kernel.Bind<IReadRepository<Owner>>().To<ReadRepository<Owner>>();
            _kernel.Bind<IWriteRepository<Owner>>().To<WriteRepository<Owner>>();
            _kernel.Bind<IWriteRepository<Animal>>().To<WriteRepository<Animal>>();
            _kernel.Bind<GameContext>().To<GameContext>().InSingletonScope();
            _kernel.Bind<IUserQuery>().To<UserQuery>();
            _kernel.Bind<IReadRepository<Animal>>().To<ReadRepository<Animal>>();
        }
    }
}
