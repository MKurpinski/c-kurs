﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using MichalKurpinskiMyGame.App_Start;
using MichalKurpinskiMyGame.View;
using Ninject;

namespace MichalKurpinskiMyGame
{
    static class Program
    {
        private static readonly IKernel Kernel;

        static Program()
        {
            Kernel = new StandardKernel();
        }
        /// <summary>
        /// The main entry point for the application. Uruchamia Forma z Grą, jeśli użytkownik został poprawnie zalogowany.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Application.EnableVisualStyles(); umożliwia nam to zmianę koloru progressBarów.
            Application.SetCompatibleTextRenderingDefault(false);
            NinjectContainer nc = new NinjectContainer(Kernel);
            nc.Load();
            var form = Kernel.Get<FormLogin>();
            Application.Run(form);
            if (form.signedIn)
            {
                FormGame formGame = Kernel.Get<FormGame>();
                formGame.LoggedOwner = form.LoggedOwner;
                formGame.CurrentAnimal = form.CurrentAnimal;
                Application.Run(formGame);
            }
        }
    }
}
