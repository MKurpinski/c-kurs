﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace MichalKurpinskiMyGame.View
{
    public partial class FormChangePassword : Form
    {
        private FormGame _owner;
        public FormChangePassword(FormGame owner)
        {
            _owner = owner;
            InitializeComponent();
        }
        /// <summary>
        /// anulowanie zmiany hasła.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Zmiana hasła.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            if (textBoxPassword.Text != "" && textBoxPassword.Text == textBoxNewPassword.Text)
            {
                _owner.LoggedOwner.Password = textBoxPassword.Text;
                _owner._writeRepository.Update(_owner.LoggedOwner);
                this.Close();
                MessageBox.Show("Udało się, hasło zostało zmienione");
            }
            else
            {
                FormGame.error.Play();
                MessageBox.Show("Zmiana hasła się nie powiodła");
            }
        }
    }
}
