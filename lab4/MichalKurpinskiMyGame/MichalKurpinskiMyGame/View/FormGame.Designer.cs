﻿namespace MichalKurpinskiMyGame.View
{
    partial class FormGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormGame));
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelWallet = new System.Windows.Forms.Label();
            this.progressBarHealth = new System.Windows.Forms.ProgressBar();
            this.progressBarHungry = new System.Windows.Forms.ProgressBar();
            this.progressBarClean = new System.Windows.Forms.ProgressBar();
            this.progressBarHappy = new System.Windows.Forms.ProgressBar();
            this.labelHealth = new System.Windows.Forms.Label();
            this.labelHunger = new System.Windows.Forms.Label();
            this.labelClean = new System.Windows.Forms.Label();
            this.labelHappines = new System.Windows.Forms.Label();
            this.buttonHealth = new System.Windows.Forms.Button();
            this.buttonFeed = new System.Windows.Forms.Button();
            this.buttonClean = new System.Windows.Forms.Button();
            this.buttonHappy = new System.Windows.Forms.Button();
            this.labelScore = new System.Windows.Forms.Label();
            this.labelAge = new System.Windows.Forms.Label();
            this.buttonFreeze = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonStartTime = new System.Windows.Forms.Button();
            this.menuStripMenu = new System.Windows.Forms.MenuStrip();
            this.użytkownikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zmieńHasłoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zmieńLoginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pomocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBoxAnimal = new System.Windows.Forms.PictureBox();
            this.menuStripMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAnimal)).BeginInit();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTitle.ForeColor = System.Drawing.Color.Black;
            this.labelTitle.Location = new System.Drawing.Point(687, 60);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(613, 54);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "Cześć to ja Twój zwierzak: ";
            // 
            // labelWallet
            // 
            this.labelWallet.AutoSize = true;
            this.labelWallet.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWallet.Location = new System.Drawing.Point(1621, 60);
            this.labelWallet.Name = "labelWallet";
            this.labelWallet.Size = new System.Drawing.Size(266, 54);
            this.labelWallet.TabIndex = 1;
            this.labelWallet.Text = "Stan Konta:";
            // 
            // progressBarHealth
            // 
            this.progressBarHealth.BackColor = System.Drawing.Color.Ivory;
            this.progressBarHealth.ForeColor = System.Drawing.Color.Green;
            this.progressBarHealth.Location = new System.Drawing.Point(338, 651);
            this.progressBarHealth.MarqueeAnimationSpeed = 10000;
            this.progressBarHealth.Name = "progressBarHealth";
            this.progressBarHealth.RightToLeftLayout = true;
            this.progressBarHealth.Size = new System.Drawing.Size(1695, 65);
            this.progressBarHealth.Step = 0;
            this.progressBarHealth.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBarHealth.TabIndex = 2;
            this.progressBarHealth.Value = 100;
            // 
            // progressBarHungry
            // 
            this.progressBarHungry.BackColor = System.Drawing.Color.Ivory;
            this.progressBarHungry.ForeColor = System.Drawing.Color.Green;
            this.progressBarHungry.Location = new System.Drawing.Point(338, 782);
            this.progressBarHungry.MarqueeAnimationSpeed = 10000;
            this.progressBarHungry.Name = "progressBarHungry";
            this.progressBarHungry.Size = new System.Drawing.Size(1695, 65);
            this.progressBarHungry.Step = 0;
            this.progressBarHungry.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBarHungry.TabIndex = 3;
            // 
            // progressBarClean
            // 
            this.progressBarClean.BackColor = System.Drawing.Color.Ivory;
            this.progressBarClean.ForeColor = System.Drawing.Color.Green;
            this.progressBarClean.Location = new System.Drawing.Point(338, 918);
            this.progressBarClean.MarqueeAnimationSpeed = 10000;
            this.progressBarClean.Name = "progressBarClean";
            this.progressBarClean.Size = new System.Drawing.Size(1695, 65);
            this.progressBarClean.Step = 0;
            this.progressBarClean.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBarClean.TabIndex = 4;
            this.progressBarClean.Value = 100;
            // 
            // progressBarHappy
            // 
            this.progressBarHappy.BackColor = System.Drawing.Color.Ivory;
            this.progressBarHappy.ForeColor = System.Drawing.Color.Green;
            this.progressBarHappy.Location = new System.Drawing.Point(338, 1053);
            this.progressBarHappy.MarqueeAnimationSpeed = 1000;
            this.progressBarHappy.Name = "progressBarHappy";
            this.progressBarHappy.Size = new System.Drawing.Size(1695, 65);
            this.progressBarHappy.Step = 2;
            this.progressBarHappy.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBarHappy.TabIndex = 5;
            this.progressBarHappy.Value = 100;
            // 
            // labelHealth
            // 
            this.labelHealth.AutoSize = true;
            this.labelHealth.Location = new System.Drawing.Point(386, 586);
            this.labelHealth.Name = "labelHealth";
            this.labelHealth.Size = new System.Drawing.Size(98, 32);
            this.labelHealth.TabIndex = 6;
            this.labelHealth.Text = "Health";
            // 
            // labelHunger
            // 
            this.labelHunger.AutoSize = true;
            this.labelHunger.Location = new System.Drawing.Point(386, 734);
            this.labelHunger.Name = "labelHunger";
            this.labelHunger.Size = new System.Drawing.Size(108, 32);
            this.labelHunger.TabIndex = 7;
            this.labelHunger.Text = "Hunger";
            // 
            // labelClean
            // 
            this.labelClean.AutoSize = true;
            this.labelClean.Location = new System.Drawing.Point(386, 867);
            this.labelClean.Name = "labelClean";
            this.labelClean.Size = new System.Drawing.Size(164, 32);
            this.labelClean.TabIndex = 8;
            this.labelClean.Text = "Cleanliness";
            // 
            // labelHappines
            // 
            this.labelHappines.AutoSize = true;
            this.labelHappines.Location = new System.Drawing.Point(386, 1002);
            this.labelHappines.Name = "labelHappines";
            this.labelHappines.Size = new System.Drawing.Size(150, 32);
            this.labelHappines.TabIndex = 9;
            this.labelHappines.Text = "Happiness";
            // 
            // buttonHealth
            // 
            this.buttonHealth.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonHealth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonHealth.Location = new System.Drawing.Point(53, 651);
            this.buttonHealth.Name = "buttonHealth";
            this.buttonHealth.Size = new System.Drawing.Size(191, 65);
            this.buttonHealth.TabIndex = 10;
            this.buttonHealth.Text = "Improve";
            this.buttonHealth.UseVisualStyleBackColor = false;
            this.buttonHealth.Click += new System.EventHandler(this.buttonHealth_Click);
            // 
            // buttonFeed
            // 
            this.buttonFeed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonFeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonFeed.Location = new System.Drawing.Point(53, 782);
            this.buttonFeed.Name = "buttonFeed";
            this.buttonFeed.Size = new System.Drawing.Size(191, 65);
            this.buttonFeed.TabIndex = 11;
            this.buttonFeed.Text = "Feed";
            this.buttonFeed.UseVisualStyleBackColor = false;
            this.buttonFeed.Click += new System.EventHandler(this.buttonFeed_Click);
            // 
            // buttonClean
            // 
            this.buttonClean.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonClean.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonClean.Location = new System.Drawing.Point(53, 918);
            this.buttonClean.Name = "buttonClean";
            this.buttonClean.Size = new System.Drawing.Size(191, 65);
            this.buttonClean.TabIndex = 12;
            this.buttonClean.Text = "Clean";
            this.buttonClean.UseVisualStyleBackColor = false;
            this.buttonClean.Click += new System.EventHandler(this.buttonClean_Click);
            // 
            // buttonHappy
            // 
            this.buttonHappy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonHappy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonHappy.Location = new System.Drawing.Point(53, 1053);
            this.buttonHappy.Name = "buttonHappy";
            this.buttonHappy.Size = new System.Drawing.Size(191, 65);
            this.buttonHappy.TabIndex = 13;
            this.buttonHappy.Text = "Have fun!";
            this.buttonHappy.UseVisualStyleBackColor = false;
            this.buttonHappy.Click += new System.EventHandler(this.buttonHappy_Click);
            // 
            // labelScore
            // 
            this.labelScore.AutoSize = true;
            this.labelScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelScore.Location = new System.Drawing.Point(38, 60);
            this.labelScore.Name = "labelScore";
            this.labelScore.Size = new System.Drawing.Size(124, 39);
            this.labelScore.TabIndex = 14;
            this.labelScore.Text = "Score: ";
            // 
            // labelAge
            // 
            this.labelAge.AutoSize = true;
            this.labelAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAge.Location = new System.Drawing.Point(1630, 187);
            this.labelAge.Name = "labelAge";
            this.labelAge.Size = new System.Drawing.Size(169, 52);
            this.labelAge.TabIndex = 15;
            this.labelAge.Text = "Wiek: 0";
            // 
            // buttonFreeze
            // 
            this.buttonFreeze.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.buttonFreeze.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonFreeze.Location = new System.Drawing.Point(1639, 475);
            this.buttonFreeze.Name = "buttonFreeze";
            this.buttonFreeze.Size = new System.Drawing.Size(394, 66);
            this.buttonFreeze.TabIndex = 16;
            this.buttonFreeze.Text = "Freeze time!<3";
            this.buttonFreeze.UseVisualStyleBackColor = false;
            this.buttonFreeze.Click += new System.EventHandler(this.buttonFreeze_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.BackColor = System.Drawing.Color.IndianRed;
            this.buttonExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonExit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonExit.Location = new System.Drawing.Point(1997, 1);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(132, 66);
            this.buttonExit.TabIndex = 17;
            this.buttonExit.Text = "EXIT";
            this.buttonExit.UseVisualStyleBackColor = false;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonStartTime
            // 
            this.buttonStartTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonStartTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonStartTime.Location = new System.Drawing.Point(1639, 568);
            this.buttonStartTime.Name = "buttonStartTime";
            this.buttonStartTime.Size = new System.Drawing.Size(394, 66);
            this.buttonStartTime.TabIndex = 18;
            this.buttonStartTime.Text = "Start Time :(";
            this.buttonStartTime.UseVisualStyleBackColor = false;
            this.buttonStartTime.Click += new System.EventHandler(this.buttonStartTime_Click);
            // 
            // menuStripMenu
            // 
            this.menuStripMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.menuStripMenu.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.menuStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.użytkownikToolStripMenuItem,
            this.pomocToolStripMenuItem});
            this.menuStripMenu.Location = new System.Drawing.Point(0, 0);
            this.menuStripMenu.Name = "menuStripMenu";
            this.menuStripMenu.Size = new System.Drawing.Size(2131, 49);
            this.menuStripMenu.TabIndex = 21;
            this.menuStripMenu.Text = "menuStrip2";
            // 
            // użytkownikToolStripMenuItem
            // 
            this.użytkownikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zmieńHasłoToolStripMenuItem,
            this.zmieńLoginToolStripMenuItem});
            this.użytkownikToolStripMenuItem.Name = "użytkownikToolStripMenuItem";
            this.użytkownikToolStripMenuItem.Size = new System.Drawing.Size(184, 45);
            this.użytkownikToolStripMenuItem.Text = "Użytkownik";
            // 
            // zmieńHasłoToolStripMenuItem
            // 
            this.zmieńHasłoToolStripMenuItem.Name = "zmieńHasłoToolStripMenuItem";
            this.zmieńHasłoToolStripMenuItem.Size = new System.Drawing.Size(307, 46);
            this.zmieńHasłoToolStripMenuItem.Text = "Zmień  Hasło";
            this.zmieńHasłoToolStripMenuItem.Click += new System.EventHandler(this.zmieńHasłoToolStripMenuItem_Click);
            // 
            // zmieńLoginToolStripMenuItem
            // 
            this.zmieńLoginToolStripMenuItem.Name = "zmieńLoginToolStripMenuItem";
            this.zmieńLoginToolStripMenuItem.Size = new System.Drawing.Size(307, 46);
            this.zmieńLoginToolStripMenuItem.Text = "Zmień Login";
            this.zmieńLoginToolStripMenuItem.Click += new System.EventHandler(this.zmieńLoginToolStripMenuItem_Click);
            // 
            // pomocToolStripMenuItem
            // 
            this.pomocToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.infoToolStripMenuItem});
            this.pomocToolStripMenuItem.Name = "pomocToolStripMenuItem";
            this.pomocToolStripMenuItem.Size = new System.Drawing.Size(122, 45);
            this.pomocToolStripMenuItem.Text = "Pomoc";
            // 
            // infoToolStripMenuItem
            // 
            this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
            this.infoToolStripMenuItem.Size = new System.Drawing.Size(185, 46);
            this.infoToolStripMenuItem.Text = "Info";
            this.infoToolStripMenuItem.Click += new System.EventHandler(this.infoToolStripMenuItem_Click);
            // 
            // pictureBoxAnimal
            // 
            this.pictureBoxAnimal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxAnimal.Location = new System.Drawing.Point(424, 135);
            this.pictureBoxAnimal.Name = "pictureBoxAnimal";
            this.pictureBoxAnimal.Size = new System.Drawing.Size(1180, 432);
            this.pictureBoxAnimal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxAnimal.TabIndex = 19;
            this.pictureBoxAnimal.TabStop = false;
            // 
            // FormGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Cornsilk;
            this.ClientSize = new System.Drawing.Size(2131, 1157);
            this.Controls.Add(this.pictureBoxAnimal);
            this.Controls.Add(this.buttonStartTime);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonFreeze);
            this.Controls.Add(this.labelAge);
            this.Controls.Add(this.labelScore);
            this.Controls.Add(this.buttonHappy);
            this.Controls.Add(this.buttonClean);
            this.Controls.Add(this.buttonFeed);
            this.Controls.Add(this.buttonHealth);
            this.Controls.Add(this.labelHappines);
            this.Controls.Add(this.labelClean);
            this.Controls.Add(this.labelHunger);
            this.Controls.Add(this.labelHealth);
            this.Controls.Add(this.progressBarHappy);
            this.Controls.Add(this.progressBarClean);
            this.Controls.Add(this.progressBarHungry);
            this.Controls.Add(this.progressBarHealth);
            this.Controls.Add(this.labelWallet);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.menuStripMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormGame";
            this.Text = "Tutaj dzieje się wszystko!";
            this.Load += new System.EventHandler(this.FormGame_Load);
            this.MouseHover += new System.EventHandler(this.FormGame_MouseHover);
            this.menuStripMenu.ResumeLayout(false);
            this.menuStripMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAnimal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelWallet;
        public System.Windows.Forms.ProgressBar progressBarHealth;
        public System.Windows.Forms.ProgressBar progressBarHungry;
        public System.Windows.Forms.ProgressBar progressBarClean;
        public System.Windows.Forms.ProgressBar progressBarHappy;
        private System.Windows.Forms.Label labelHealth;
        private System.Windows.Forms.Label labelHunger;
        private System.Windows.Forms.Label labelClean;
        private System.Windows.Forms.Label labelHappines;
        private System.Windows.Forms.Button buttonHealth;
        private System.Windows.Forms.Button buttonFeed;
        private System.Windows.Forms.Button buttonClean;
        private System.Windows.Forms.Button buttonHappy;
        private System.Windows.Forms.Label labelScore;
        private System.Windows.Forms.Label labelAge;
        private System.Windows.Forms.Button buttonFreeze;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonStartTime;
        private System.Windows.Forms.PictureBox pictureBoxAnimal;
        private System.Windows.Forms.MenuStrip menuStripMenu;
        private System.Windows.Forms.ToolStripMenuItem użytkownikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zmieńHasłoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zmieńLoginToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pomocToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
    }
}