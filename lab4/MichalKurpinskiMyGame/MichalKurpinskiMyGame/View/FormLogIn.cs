﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MichalKurpinskiMyGame.Model;
using MichalKurpinskiMyGame.Repository.Command.Interfaces;
using MichalKurpinskiMyGame.Repository.Query;
using MichalKurpinskiMyGame.Repository.Query.Interfaces;
using MichalKurpinskiMyGame.View;
using Ninject;

namespace MichalKurpinskiMyGame
{
    public partial class FormLogin : Form
    {
        public bool signedIn = false;
        SoundPlayer fanfares = new SoundPlayer(Properties.Resources.Fanfary);
        public Owner LoggedOwner = new Owner();
        public Animal CurrentAnimal= new Animal();
        private readonly IReadRepository<Animal> _animalReadRepository;
        private readonly IUserQuery _query;
        private readonly IReadRepository<Owner> _readRepository;
        private readonly IWriteRepository<Owner> _writeRepository;
        private readonly IWriteRepository<Animal> _animalWriteRepository;
        private readonly IKernel _kernel;
        private readonly GameContext _context;
        public FormLogin(

            GameContext context,
            IReadRepository<Animal> animalReadRepository,
            IWriteRepository<Animal> animalWriteRepository,
            IUserQuery query,
            IReadRepository<Owner> readRepository,
            IWriteRepository<Owner> writeRepository,
            IKernel kernel)
        {
            _animalReadRepository = animalReadRepository;
            _context = context;
            _query = query;
            _readRepository = readRepository;
            _writeRepository = writeRepository;
            _kernel = kernel;
            _animalWriteRepository = animalWriteRepository;
            InitializeComponent();
        }
        /// <summary>
        /// Rejestracja użytkownika do bazy Danych.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSignUp_Click(object sender, EventArgs e)
        {
            if (textBoxLogin.Text == "" || textBoxPassword.Text == "" || textBoxAnimalName.Text == "")
            {
                FormGame.error.Play();
                MessageBox.Show("Wpisz coś!:D", "Spróbuj jeszcze raz!");
                return;
            }
            if (_query.GetByLogin(textBoxLogin.Text)==null)
            {
                Animal animal = new Animal()
                {
                    Name = textBoxAnimalName.Text,
                    Health = 100,//Początkowe wartosci i maksymalne/minimalne wartosci atrybutów zwierzątka.
                    Happiness = 100,
                    Cleanliness = 100,
                    Age = 0,
                    Hungry = 0
                };
                _animalWriteRepository.Save(animal);
                _context.SaveChanges();
                
                Owner owner = new Owner()
                {
                    Name = textBoxLogin.Text,
                    AnimalId = animal.Id,
                    Password = textBoxPassword.Text,
                    Wallet = 100,
                    Score = 0
                };
                _writeRepository.Save(owner);
                _context.SaveChanges();
                fanfares.Play();
                MessageBox.Show("Udało się, teraz wystarczy się tylko zalogować.", "Zaczynamy?");
            }
            else
            {
                FormGame.error.Play();
                MessageBox.Show("Uźytkownik o podanym loginie już istnieje!", "Spróbuj jeszcze raz");
            }
        }
        /// <summary>
        /// Logowanie użytkownika do aplikacji.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSignIn_Click(object sender, EventArgs e)
        {
            if (_query.GetOwnerByLoginAndPassword(textBoxLogin.Text, textBoxPassword.Text)!=null)
            {
                signedIn = true;
                LoggedOwner = (_query.GetByLogin(textBoxLogin.Text));
                CurrentAnimal = _animalReadRepository.GetById(LoggedOwner.AnimalId);
                fanfares.Play();
                MessageBox.Show("Udało się, zaczynamy zabawe!", "Gotów?");
                this.Close();
            }
            else
            {
                FormGame.error.Play();
                MessageBox.Show("Niepoprawne hasło lub login", "Ups..");
            }
        }

        private void buttonTopScore_Click(object sender, EventArgs e)
        {
            FormTop formTop = _kernel.Get<FormTop>();
            formTop.Show();
        }

    }
}
