﻿using System;
using System.Drawing;
using System.Media;
using System.Windows.Forms;
using MichalKurpinskiMyGame.Model;
using MichalKurpinskiMyGame.Repository.Command.Interfaces;
using MichalKurpinskiMyGame.Repository.Query.Interfaces;
using Ninject;
using Timer = System.Windows.Forms.Timer;

namespace MichalKurpinskiMyGame.View
{
    public partial class FormGame : Form
    {
        private int maxHealth = 100;
        private int maxClean = 100;
        private int maxHappy = 100;
        private int minHungry = 0;
        private int minWallet = 0;
        public static SystemSound error = SystemSounds.Hand;
        private int freezed = 0;
        private int ticks;
        public Timer timer = new Timer();
        public Timer timerScore = new Timer();
        public Owner LoggedOwner { get; set; }
        public Animal CurrentAnimal { get; set; }
        public readonly IUserQuery _query;
        private readonly IReadRepository<Owner> _readRepository;
        public readonly IWriteRepository<Owner> _writeRepository;
        private readonly IWriteRepository<Animal> _animalWriteRepository;
        private readonly IKernel _kernel;
        private readonly GameContext _context;

        public FormGame(
            GameContext context,
            IWriteRepository<Animal> animalWriteRepository,
            IUserQuery query,
            IReadRepository<Owner> readRepository,
            IWriteRepository<Owner> writeRepository,
            IKernel kernel)
        {
            _context = context;
            _query = query;
            _readRepository = readRepository;
            _writeRepository = writeRepository;
            _kernel = kernel;
            _animalWriteRepository = animalWriteRepository;
            InitializeComponent();
        }
        /// <summary>
        /// Ustawia startowe wartości.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormGame_Load(object sender, EventArgs e)
        {
            labelTitle.Text = "Cześć mam na imię: " + CurrentAnimal.Name;
            labelWallet.Text = "Stan Konta: " + LoggedOwner.Wallet;
            labelScore.Text = "Score: " + LoggedOwner.Score;
            InitializeMyTimer();
            progressBarClean.Value = CurrentAnimal.Cleanliness;
            progressBarHappy.Value = CurrentAnimal.Happiness;
            progressBarHealth.Value = CurrentAnimal.Health;
            progressBarHungry.Value = CurrentAnimal.Hungry;
            ChangeImage(Properties.Resources.Piesek1);
        }
        /// <summary>
        /// Aktualizuje statystki zwierzątka.
        /// </summary>
        private void AnimalValues()
        {
            CurrentAnimal.Cleanliness = progressBarClean.Value;
            CurrentAnimal.Happiness = progressBarHappy.Value;
            CurrentAnimal.Hungry = progressBarHungry.Value;
            CurrentAnimal.Health = progressBarHealth.Value;
            _animalWriteRepository.Update(CurrentAnimal);
            _context.SaveChanges();
            
        }
        /// <summary>
        /// Aktualizuje stan konta użytkownika.
        /// </summary>
        /// <param name="x"></param>
        private void UpdateWallet(int x)
        {
            LoggedOwner.Wallet += x;
            _writeRepository.Update(LoggedOwner);
            _context.SaveChanges();
            labelWallet.Text = "Stan Konta: " + LoggedOwner.Wallet;
        }
        /// <summary>
        /// Sprawdz czy zwięrzę powinno już odejsć od użytkownika.
        /// </summary>
        private void CheckAnimalValues()
        {
            if (
                CurrentAnimal.Cleanliness == 0 || CurrentAnimal.Happiness == 0 ||//Stykowe warunki, jeśli osiagną one swoje maksymalne/minimalne wartosci to zwierzę odchodzi.
                CurrentAnimal.Hungry == 100 || CurrentAnimal.Health == 0
                )
            {
                timer.Stop();
                timerScore.Stop();
                ChangeImage(Properties.Resources.Bye);
                error.Play();
                MessageBox.Show("Twoje zwierzątko odeszło do innego własciciela, koniec gry:(. Twój wynik to:  "+ LoggedOwner.Score, "To już jest koniec..");
                this.Close();
            }
        }
        /// <summary>
        /// inicjuje Timery.
        /// </summary>
        private void InitializeMyTimer()
        {
            // Set the interval for the timer.
            timer.Interval =1000;
            timerScore.Interval = 100;
            // Connect the Tick event of the timer to its event handler.
            timer.Tick += new EventHandler(DecreaseProgressBar);
            timerScore.Tick += new EventHandler(Score);
            timer.Tick += new EventHandler(ChangeProgressBarColor);
            // Start the timer.
            timer.Start();
            timerScore.Start();
        }
        /// <summary>
        /// Aktualizowanie statusów progressBarów.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DecreaseProgressBar(object sender, EventArgs e)
        {
            progressBarClean.Increment(-1);
            progressBarHealth.Increment(-1);
            progressBarHappy.Increment(-1);
            progressBarHungry.Increment(1);
            AnimalValues();
            CheckAnimalValues();

        }
        /// <summary>
        /// Sprawdza czy przyznać pięniądze użytkownikowi.
        /// </summary>
        private void FreeMoney()
        {
            if (ticks%100 == 0)
            {
                UpdateWallet(2);
            }
        }
        /// <summary>
        /// Aktualizuje wynik użytkownika.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Score(object sender, EventArgs e)
        {
            
            labelScore.Text = "Score: " + LoggedOwner.Score;
            LoggedOwner.Score += 1;
            ticks += 1;
            FreeMoney();
            UpdateAge();
            _context.SaveChanges();
        }
        /// <summary>
        /// Polepsza statystykę zdrowia.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonHealth_Click(object sender, EventArgs e)
        {
            if (progressBarHealth.Value <= maxHealth-2 && LoggedOwner.Wallet >= 5) //Sprawdzenie warunków na mozliwość kupna
            {
                progressBarHealth.Increment(2);
                AnimalValues();
                UpdateWallet(-5);
                ChangeImage(Properties.Resources.vet);
            }
            else
            {
                error.Play();
                MessageBox.Show("Osiągniety maksymalny poziom lub nie masz wystarczająco dużo pięniedzy.");
            }
        }
        /// <summary>
        /// Polepsza statystykę sytosci.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFeed_Click(object sender, EventArgs e)
        {
            if (progressBarHungry.Value >= minHungry+2 && LoggedOwner.Wallet >= minWallet +2)
            {
                progressBarHungry.Increment(-2);
                AnimalValues();
                UpdateWallet(-2);
                ChangeImage(Properties.Resources.JedzacyPies);
            }
            else
            {
                error.Play();
                MessageBox.Show("Osiągniety minimalny poziom lub nie masz wystarczająco dużo pięniedzy.");
            }
        }
        /// <summary>
        /// Polepsza statystykę czystości.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClean_Click(object sender, EventArgs e)
        {
            if (progressBarClean.Value <= maxClean-2 && LoggedOwner.Wallet >= minWallet+3)
            {
                progressBarClean.Increment(2);
                AnimalValues();
                UpdateWallet(-3);
                ChangeImage(Properties.Resources.MytyPiesek);
            }
            else
            {
                error.Play();
                MessageBox.Show("Osiągniety maksymalny poziom lub nie masz wystarczająco dużo pięniedzy.");
            }
        }
        /// <summary>
        /// Podwyższa statystykę szczęscia.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonHappy_Click(object sender, EventArgs e)
        {
            if (progressBarHappy.Value <= maxHappy-2 && LoggedOwner.Wallet >= minWallet+1)
            {
                progressBarHappy.Increment(2);
                AnimalValues();
                UpdateWallet(-1);
                ChangeImage(Properties.Resources.DJ_Dog);
            }
            else
            {
                error.Play();
                MessageBox.Show("Osiągniety maksymalny poziom lub nie masz wystarczająco dużo pięniedzy.");
            }
        }
        /// <summary>
        /// Ualtualnia wiek zwierzęcia w Bazie danych.
        /// </summary>
        private void UpdateAge()
        {
            if (ticks%600 == 0)
            {
                CurrentAnimal.Age += 1;
                _animalWriteRepository.Update(CurrentAnimal);
                labelAge.Text = "Wiek: " + CurrentAnimal.Age;
            }
        }
        /// <summary>
        /// Zamyka aplikacje.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// "Zamraza" czas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFreeze_Click(object sender, EventArgs e)
        {
            if (freezed < 5 && LoggedOwner.Wallet>=(minWallet+10)) // Można zamrozić czas tylko 5 razy!
            {
                timer.Enabled = false;
                UpdateWallet(-10);
                freezed++;
            }
            else
            {
                error.Play();
                MessageBox.Show("Czas można zamrozić tylko 5 razy! Już to zrobiłeś albo masz za mało pieniedzy!",
                    "Dasz rade!");
            }
        }
        /// <summary>
        /// Otwiera okno zmiany hasła.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void zmieńHasłoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormChangePassword formChangePassword = new FormChangePassword(this);
            formChangePassword.Show();
        }
        /// <summary>
        /// Otwiera Okno zmiany loginu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void zmieńLoginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormNewLogin formNewLogin = new FormNewLogin(this);
            formNewLogin.Show();
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                "Jeśli tu trafiłes to oznacza to, że pomyślnie przeszedłeś proces rejestracji, brawo!:)\n Gra polega na utrzymaniu w posiadaniu naszego zwierzaczka(coś w stylu znanego Tamagothi.)\n" +
                "Zadady gry są proste: \n" +
                "1.W miarę upływu czasu zmieniają się atrybuty życiowe zwierzaczka(na naszą niekorzyść.)\n" +
                "2.Co określony okres czasu do naszego portfela wpływa pewna ilosc pieniedzy w ramach rekompensaty za naszą opiekę,\n" +
                "3.Można zamrozić czas tylko 5 razy w ciągu gry(koszt 10 monet),\n" +
                "4.Przycisk zdrowia poprawia zdrowie naszego zwierzaka o 2/Koszt 5 monet,\n" +
                "5.Przycisk szczescia poprawia szczęsce o 2/koszt 1 moneta,\n" +
                "6.Przycisk karmienia zmiejsza głod o 2/koszt 2 monety,\n" +
                "7.Przycisk czystosci zwieksza czystosc o 2/Koszt 3 monety.\n"+
                "Powodzenia!");
        }

        /// <summary>
        /// "Odmraża" czas.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonStartTime_Click(object sender, EventArgs e)
        {
            timer.Enabled = true;
        }
        /// <summary>
        /// Zmienia obrazek w PictureBoxie.
        /// </summary>
        /// <param name="image"></param>
        private void ChangeImage(Bitmap image)
        {
            pictureBoxAnimal.Image = image;
        }
        /// <summary>
        /// Zmienianie obrazka w PictureBoxie, gdy mysz zostanie na formie w bezruchu, używana to zmiany obrazka po nacisnieciu jednego z przycisków(do polepszania statystyk).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormGame_MouseHover(object sender, EventArgs e)
        {
            ChangeImage(Properties.Resources.Piesek1);
        }
        /// <summary>
        /// Zmienia kolor progressBaru jeśli jego poziom schodzi poniżej określonej wartosci.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeProgressBarColor(Object sender, EventArgs e)
        {
            foreach (Control control in Controls)
            {
                if (control is ProgressBar)
                {
                    ChangeColorHelp(control as ProgressBar);
                }
            }
            
        }
        /// <summary>
        /// Metoda pomocnicza do zmieniania koloru progressBaru.
        /// </summary>
        /// <param name="progressBar"></param>
        private void ChangeColorHelp(ProgressBar progressBar)
        {
            if (progressBar == progressBarHungry && progressBar.Value > maxHappy-20)
            {
                progressBar.ForeColor = Color.Red;
            }
            else if (progressBar == progressBarHungry)
            {
                progressBar.ForeColor = Color.Green;
            }
            else if (progressBar.Value > minHungry+20)
            {
                progressBar.ForeColor = Color.Green;
            }
            else
            {
                progressBar.ForeColor = Color.Red;
            }
        }
    }
}
