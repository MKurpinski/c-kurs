﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiMyGame.View
{
    public partial class FormNewLogin : Form
    {
        private FormGame _owner;
        public FormNewLogin(FormGame owner)
        {
            _owner = owner;
            InitializeComponent();
        }
        /// <summary>
        /// Anulowanie zmiany loginu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Potwierdzenie zmiany loginu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            if (_owner._query.GetByLogin(textBoxLogin.Text)!=null && textBoxLogin.Text != "" && textBoxLogin.Text == textBoxNewLogin.Text)
            {
                _owner.LoggedOwner.Name = textBoxLogin.Text;
                _owner._writeRepository.Update(_owner.LoggedOwner);
                this.Close();
                MessageBox.Show("Udało się, login został zmieniony");
            }
            else
            {
                FormGame.error.Play();
                MessageBox.Show("Zmiana loginu się nie powiodła");
            }
        }
    }
}
