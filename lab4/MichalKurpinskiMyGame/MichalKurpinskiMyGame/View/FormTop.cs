﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MichalKurpinskiMyGame.Model;
using MichalKurpinskiMyGame.Repository.Command.Interfaces;
using MichalKurpinskiMyGame.Repository.Query.Interfaces;

namespace MichalKurpinskiMyGame.View
{
    public partial class FormTop : Form
    {
        private readonly IUserQuery _userQuery;
        public FormTop(IUserQuery userQuery)
        {
            _userQuery = userQuery;
            InitializeComponent();
        }

        private void FormTop_Load(object sender, EventArgs e)
        {
            dataGridViewTopScore.DataSource = _userQuery.GetAll();
            dataGridViewTopScore.Columns[0].HeaderText = "Login";
            dataGridViewTopScore.Columns[1].Visible = false;
            dataGridViewTopScore.Columns[2].Visible = false;
            dataGridViewTopScore.Columns[3].Visible = false;
            dataGridViewTopScore.Columns[5].Visible = false;
        }
    }
}
