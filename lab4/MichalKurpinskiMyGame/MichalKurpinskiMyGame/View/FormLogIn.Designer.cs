﻿namespace MichalKurpinskiMyGame
{
    partial class FormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLogin));
            this.buttonSignIn = new System.Windows.Forms.Button();
            this.buttonSignUp = new System.Windows.Forms.Button();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelLogin = new System.Windows.Forms.Label();
            this.labelPassword = new System.Windows.Forms.Label();
            this.labelNameOfAnimal = new System.Windows.Forms.Label();
            this.textBoxLogin = new System.Windows.Forms.TextBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.textBoxAnimalName = new System.Windows.Forms.TextBox();
            this.buttonTopScore = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonSignIn
            // 
            this.buttonSignIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonSignIn.Location = new System.Drawing.Point(48, 589);
            this.buttonSignIn.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.buttonSignIn.Name = "buttonSignIn";
            this.buttonSignIn.Size = new System.Drawing.Size(336, 100);
            this.buttonSignIn.TabIndex = 0;
            this.buttonSignIn.Text = "Sign In";
            this.buttonSignIn.UseVisualStyleBackColor = false;
            this.buttonSignIn.Click += new System.EventHandler(this.buttonSignIn_Click);
            // 
            // buttonSignUp
            // 
            this.buttonSignUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonSignUp.Location = new System.Drawing.Point(424, 589);
            this.buttonSignUp.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.buttonSignUp.Name = "buttonSignUp";
            this.buttonSignUp.Size = new System.Drawing.Size(352, 100);
            this.buttonSignUp.TabIndex = 1;
            this.buttonSignUp.Text = "Sign Up";
            this.buttonSignUp.UseVisualStyleBackColor = false;
            this.buttonSignUp.Click += new System.EventHandler(this.buttonSignUp_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTitle.Location = new System.Drawing.Point(32, 24);
            this.labelTitle.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(736, 76);
            this.labelTitle.TabIndex = 2;
            this.labelTitle.Text = "Wejdź w świat zwierząt!";
            // 
            // labelLogin
            // 
            this.labelLogin.AutoSize = true;
            this.labelLogin.Location = new System.Drawing.Point(48, 131);
            this.labelLogin.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(80, 31);
            this.labelLogin.TabIndex = 3;
            this.labelLogin.Text = "Login";
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(48, 255);
            this.labelPassword.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(134, 31);
            this.labelPassword.TabIndex = 4;
            this.labelPassword.Text = "Password";
            // 
            // labelNameOfAnimal
            // 
            this.labelNameOfAnimal.AutoSize = true;
            this.labelNameOfAnimal.Location = new System.Drawing.Point(48, 410);
            this.labelNameOfAnimal.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.labelNameOfAnimal.Name = "labelNameOfAnimal";
            this.labelNameOfAnimal.Size = new System.Drawing.Size(519, 31);
            this.labelNameOfAnimal.TabIndex = 5;
            this.labelNameOfAnimal.Text = "Animal Name(required only for new users)";
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxLogin.Location = new System.Drawing.Point(48, 171);
            this.textBoxLogin.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.textBoxLogin.Multiline = true;
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(724, 66);
            this.textBoxLogin.TabIndex = 6;
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxPassword.Location = new System.Drawing.Point(48, 310);
            this.textBoxPassword.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.textBoxPassword.Multiline = true;
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(724, 66);
            this.textBoxPassword.TabIndex = 7;
            // 
            // textBoxAnimalName
            // 
            this.textBoxAnimalName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxAnimalName.Location = new System.Drawing.Point(48, 472);
            this.textBoxAnimalName.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.textBoxAnimalName.Multiline = true;
            this.textBoxAnimalName.Name = "textBoxAnimalName";
            this.textBoxAnimalName.Size = new System.Drawing.Size(724, 66);
            this.textBoxAnimalName.TabIndex = 8;
            // 
            // buttonTopScore
            // 
            this.buttonTopScore.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonTopScore.Location = new System.Drawing.Point(48, 720);
            this.buttonTopScore.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.buttonTopScore.Name = "buttonTopScore";
            this.buttonTopScore.Size = new System.Drawing.Size(720, 100);
            this.buttonTopScore.TabIndex = 9;
            this.buttonTopScore.Text = "TopScore";
            this.buttonTopScore.UseVisualStyleBackColor = false;
            this.buttonTopScore.Click += new System.EventHandler(this.buttonTopScore_Click);
            // 
            // FormLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(240F, 240F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Cornsilk;
            this.ClientSize = new System.Drawing.Size(817, 866);
            this.Controls.Add(this.buttonTopScore);
            this.Controls.Add(this.textBoxAnimalName);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.textBoxLogin);
            this.Controls.Add(this.labelNameOfAnimal);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.labelLogin);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.buttonSignUp);
            this.Controls.Add(this.buttonSignIn);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.MaximizeBox = false;
            this.Name = "FormLogin";
            this.Text = "Logowanie/Rejestracja";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSignIn;
        private System.Windows.Forms.Button buttonSignUp;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Label labelNameOfAnimal;
        private System.Windows.Forms.TextBox textBoxLogin;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.TextBox textBoxAnimalName;
        private System.Windows.Forms.Button buttonTopScore;
    }
}

