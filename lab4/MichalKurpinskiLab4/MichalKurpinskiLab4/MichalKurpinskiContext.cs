namespace MichalKurpinskiLab4
{
    using Model;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class MichalKurpinskiContext : DbContext
    {

        public MichalKurpinskiContext()
            : base("name=MichalKurpinskiContext")
        {
            Database.SetInitializer<MichalKurpinskiContext>(new DropCreateDatabaseIfModelChanges<MichalKurpinskiContext>());
        }
        public virtual DbSet<Grade> Grades { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<Adress> Adresses { get; set; }
        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }


}