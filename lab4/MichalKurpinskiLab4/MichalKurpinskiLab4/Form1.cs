﻿using MichalKurpinskiLab4.Model;
using MichalKurpinskiLab4.Repozytorium.Command;
using MichalKurpinskiLab4.Repozytorium.Command.Interfaces;
using MichalKurpinskiLab4.Repozytorium.Query;
using MichalKurpinskiLab4.Repozytorium.Query.Intefaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MichalKurpinskiLab4
{
    public partial class Form1 : Form
    {
        private  IReadRepository<Student> _readStudentRepository;
        private readonly IWriteRepository <Student> _writeStudentRepository;
        private readonly MichalKurpinskiContext _context;
        private  IUserQuery _userQuery;
        public Form1()
        {
            _context = new MichalKurpinskiContext();
            _writeStudentRepository = new WriteRepository<Student>(_context);
            _readStudentRepository = new ReadRepository<Student>(_context);
            _userQuery = new UserQuery(_context);
            InitializeComponent();
        }

        private void buttonAddNewStudent_Click(object sender, EventArgs e)
        {
            Student student = new Student()
            {
                Name = textBoxAddName.Text,
                Surname = textBoxAddSurname.Text,
                Indeks = textBoxIndeks.Text,
                Adress = new Adress()
                {
                    City = textBoxCity.Text,
                    PostCode = textBoxPostCode.Text
                }
            };
            _writeStudentRepository.Create(student);
            _context.SaveChanges();
           
        }

        private void buttonShowStudents_Click(object sender, EventArgs e)
        {
            dataGridViewStudents.DataSource = null;
            dataGridViewStudents.DataSource = _userQuery.UserWithAdressByName(textBoxName.Text)
            .Select(x => new
            {
                Imie = x.Name,
                Nazwisko = x.Surname,
                Miasto = x.Adress.City,
                KodPocztowy = x.Adress.PostCode


            })
            .ToList();
        }

        
    }
}
