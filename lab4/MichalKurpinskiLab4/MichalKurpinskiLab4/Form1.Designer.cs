﻿namespace MichalKurpinskiLab4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAddNewStudent = new System.Windows.Forms.Button();
            this.dataGridViewStudents = new System.Windows.Forms.DataGridView();
            this.buttonShowStudents = new System.Windows.Forms.Button();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxAddSurname = new System.Windows.Forms.TextBox();
            this.textBoxAddName = new System.Windows.Forms.TextBox();
            this.textBoxIndeks = new System.Windows.Forms.TextBox();
            this.groupBoxStudent = new System.Windows.Forms.GroupBox();
            this.groupBoxAdress = new System.Windows.Forms.GroupBox();
            this.textBoxPostCode = new System.Windows.Forms.TextBox();
            this.textBoxCity = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudents)).BeginInit();
            this.groupBoxStudent.SuspendLayout();
            this.groupBoxAdress.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonAddNewStudent
            // 
            this.buttonAddNewStudent.Location = new System.Drawing.Point(354, 365);
            this.buttonAddNewStudent.Name = "buttonAddNewStudent";
            this.buttonAddNewStudent.Size = new System.Drawing.Size(201, 127);
            this.buttonAddNewStudent.TabIndex = 0;
            this.buttonAddNewStudent.Text = "Dodaj Studenta";
            this.buttonAddNewStudent.UseVisualStyleBackColor = true;
            this.buttonAddNewStudent.Click += new System.EventHandler(this.buttonAddNewStudent_Click);
            // 
            // dataGridViewStudents
            // 
            this.dataGridViewStudents.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewStudents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStudents.Location = new System.Drawing.Point(46, 29);
            this.dataGridViewStudents.Name = "dataGridViewStudents";
            this.dataGridViewStudents.RowTemplate.Height = 40;
            this.dataGridViewStudents.Size = new System.Drawing.Size(1495, 266);
            this.dataGridViewStudents.TabIndex = 1;
            // 
            // buttonShowStudents
            // 
            this.buttonShowStudents.Location = new System.Drawing.Point(1340, 528);
            this.buttonShowStudents.Name = "buttonShowStudents";
            this.buttonShowStudents.Size = new System.Drawing.Size(201, 127);
            this.buttonShowStudents.TabIndex = 2;
            this.buttonShowStudents.Text = "Pokaz Studentów";
            this.buttonShowStudents.UseVisualStyleBackColor = true;
            this.buttonShowStudents.Click += new System.EventHandler(this.buttonShowStudents_Click);
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(1052, 557);
            this.textBoxName.Multiline = true;
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(280, 65);
            this.textBoxName.TabIndex = 3;
            // 
            // textBoxAddSurname
            // 
            this.textBoxAddSurname.Location = new System.Drawing.Point(0, 122);
            this.textBoxAddSurname.Multiline = true;
            this.textBoxAddSurname.Name = "textBoxAddSurname";
            this.textBoxAddSurname.Size = new System.Drawing.Size(280, 65);
            this.textBoxAddSurname.TabIndex = 4;
            // 
            // textBoxAddName
            // 
            this.textBoxAddName.Location = new System.Drawing.Point(0, 38);
            this.textBoxAddName.Multiline = true;
            this.textBoxAddName.Name = "textBoxAddName";
            this.textBoxAddName.Size = new System.Drawing.Size(280, 65);
            this.textBoxAddName.TabIndex = 5;
            // 
            // textBoxIndeks
            // 
            this.textBoxIndeks.Location = new System.Drawing.Point(0, 218);
            this.textBoxIndeks.Multiline = true;
            this.textBoxIndeks.Name = "textBoxIndeks";
            this.textBoxIndeks.Size = new System.Drawing.Size(280, 65);
            this.textBoxIndeks.TabIndex = 6;
            // 
            // groupBoxStudent
            // 
            this.groupBoxStudent.Controls.Add(this.textBoxAddName);
            this.groupBoxStudent.Controls.Add(this.textBoxIndeks);
            this.groupBoxStudent.Controls.Add(this.textBoxAddSurname);
            this.groupBoxStudent.Location = new System.Drawing.Point(46, 327);
            this.groupBoxStudent.Name = "groupBoxStudent";
            this.groupBoxStudent.Size = new System.Drawing.Size(302, 328);
            this.groupBoxStudent.TabIndex = 7;
            this.groupBoxStudent.TabStop = false;
            this.groupBoxStudent.Text = "Student";
            // 
            // groupBoxAdress
            // 
            this.groupBoxAdress.Controls.Add(this.textBoxPostCode);
            this.groupBoxAdress.Controls.Add(this.textBoxCity);
            this.groupBoxAdress.Location = new System.Drawing.Point(46, 662);
            this.groupBoxAdress.Name = "groupBoxAdress";
            this.groupBoxAdress.Size = new System.Drawing.Size(302, 239);
            this.groupBoxAdress.TabIndex = 8;
            this.groupBoxAdress.TabStop = false;
            this.groupBoxAdress.Text = "Adress";
            // 
            // textBoxPostCode
            // 
            this.textBoxPostCode.Location = new System.Drawing.Point(0, 125);
            this.textBoxPostCode.Multiline = true;
            this.textBoxPostCode.Name = "textBoxPostCode";
            this.textBoxPostCode.Size = new System.Drawing.Size(280, 65);
            this.textBoxPostCode.TabIndex = 7;
            // 
            // textBoxCity
            // 
            this.textBoxCity.Location = new System.Drawing.Point(0, 37);
            this.textBoxCity.Multiline = true;
            this.textBoxCity.Name = "textBoxCity";
            this.textBoxCity.Size = new System.Drawing.Size(280, 65);
            this.textBoxCity.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1567, 913);
            this.Controls.Add(this.groupBoxAdress);
            this.Controls.Add(this.groupBoxStudent);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.buttonShowStudents);
            this.Controls.Add(this.dataGridViewStudents);
            this.Controls.Add(this.buttonAddNewStudent);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudents)).EndInit();
            this.groupBoxStudent.ResumeLayout(false);
            this.groupBoxStudent.PerformLayout();
            this.groupBoxAdress.ResumeLayout(false);
            this.groupBoxAdress.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAddNewStudent;
        private System.Windows.Forms.DataGridView dataGridViewStudents;
        private System.Windows.Forms.Button buttonShowStudents;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxAddSurname;
        private System.Windows.Forms.TextBox textBoxAddName;
        private System.Windows.Forms.TextBox textBoxIndeks;
        private System.Windows.Forms.GroupBox groupBoxStudent;
        private System.Windows.Forms.GroupBox groupBoxAdress;
        private System.Windows.Forms.TextBox textBoxPostCode;
        private System.Windows.Forms.TextBox textBoxCity;
    }
}

