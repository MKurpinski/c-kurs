﻿using MichalKurpinskiLab4.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MichalKurpinskiLab4.Repozytorium.Command.Interfaces
{
    interface IWriteRepository<T> where T:Entity
    {
        void Create(T entity);
        void Delete(T entity);
        void Edit(T entity);

    }
}
