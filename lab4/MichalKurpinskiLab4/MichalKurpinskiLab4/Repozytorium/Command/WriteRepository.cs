﻿using MichalKurpinskiLab4.Model;
using MichalKurpinskiLab4.Repozytorium.Command.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MichalKurpinskiLab4.Repozytorium.Command
{
    public class WriteRepository<T>:IWriteRepository<T> where T:Entity
    {
        private readonly MichalKurpinskiContext _context;
        public WriteRepository(MichalKurpinskiContext context)
        {
            _context = context;
        }
        public void Create(T entity)
        {
            _context.Set<T>().Add(entity);
        }

        public void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        public void Edit(T entity)
        {
            _context.Entry<T>(entity);
        }
    }
}
