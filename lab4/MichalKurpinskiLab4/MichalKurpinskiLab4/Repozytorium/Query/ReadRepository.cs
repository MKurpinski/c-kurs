﻿using MichalKurpinskiLab4.Model;
using MichalKurpinskiLab4.Repozytorium.Query.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MichalKurpinskiLab4.Repozytorium.Query
{
    public class ReadRepository<T>:IReadRepository<T> where T:Entity
    {
        protected readonly MichalKurpinskiContext _context;
        public ReadRepository(MichalKurpinskiContext context) {
            _context = context;
        }
        public IList<T> GetAll() {
           return  _context.Set<T>().ToList();
        }
        public T GetByID(int id) {
            return _context.Set<T>().Where(x => x.ID == id).FirstOrDefault();
        }
    }
}
