﻿using MichalKurpinskiLab4.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MichalKurpinskiLab4.Repozytorium.Query
{
    class UserQuery : ReadRepository<Student>, IUserQuery
    {
        public UserQuery(MichalKurpinskiContext context) : base(context) { }

        public IList<Student> UserWithAdressByName(string name)
        {
           return  _context.Set<Student>()
                .Where(q => q.Name == name)
                .ToList();
        }
        
    }
}
