﻿using MichalKurpinskiLab4.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MichalKurpinskiLab4.Repozytorium.Query.Intefaces
{
    interface IReadRepository<T> where T: Entity
    {
        IList<T> GetAll();
        T GetByID(int id);
    }
}
