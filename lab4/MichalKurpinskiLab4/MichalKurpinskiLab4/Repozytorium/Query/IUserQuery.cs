﻿using MichalKurpinskiLab4.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MichalKurpinskiLab4.Repozytorium.Query
{
    public interface IUserQuery
    {
        IList<Student> UserWithAdressByName(String name);

    }
}
