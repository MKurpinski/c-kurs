﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MichalKurpinskiLab4.Model
{
    public class Adress:Entity
    {
        public string PostCode { get; set; }
        public string City { get; set; }
    }
}
