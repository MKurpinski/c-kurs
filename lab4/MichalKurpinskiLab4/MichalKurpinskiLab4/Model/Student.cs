﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MichalKurpinskiLab4.Model
{
    public class Student: Entity
    {
        public Student() {
            Grades = new List<Grade>();
        }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Indeks { get; set; }
        //relacja jeden do wielu
        public virtual IList<Grade> Grades { get; set; }
        //relacja jeden do jeden
        public virtual Adress Adress { get; set; }


    }
}
