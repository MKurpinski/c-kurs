﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MichalKurpinskiLab4.Model
{
    public class Grade:Entity
    {
        public float Index { get; set; }
        public int StudentID { get; set; }
    }
}
