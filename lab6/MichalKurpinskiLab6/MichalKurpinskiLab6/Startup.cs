﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MichalKurpinskiLab6.Startup))]
namespace MichalKurpinskiLab6
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
